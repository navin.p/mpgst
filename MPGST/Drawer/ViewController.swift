//
//  ViewController.swift
//  WireFraming
//
//  Created by admin on 26/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    var dicoLocalisation = NSDictionary()
    var strTag : Int?
    
    @IBOutlet weak var tv: UITableView!
    private let aryMenuItems = ["Home","Tutorial Videos","Smart FAQ","Acts","Rules","Schedules","SGST Notification","GST Rates","GST Calculator","Help Desk","News Updates","Articles","Ask Our Expert","Favourites","Notifications","Disclaimer","About App","Share App","Expert Contribution","Developer Info"] as NSArray

    override func viewDidLoad() {
        super.viewDidLoad()
        tv.delegate = self
        tv.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- UITableView Delegate Methods
    //MARK:-
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return aryMenuItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != 0 {
            let cell = tv.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! DrawerCellTableViewCell
            cell.lbltitle.text = aryMenuItems[indexPath.row]as? String
            return cell
        }else{
            let cell = tv.dequeueReusableCell(withIdentifier: "MenuCellStatic", for: indexPath as IndexPath) as! DrawerCellTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 220
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == 0 {
            
            let mainVcIntial = kConstantObj.SetMainViewController (aStoryBoardID: "HomeVC")
              appDelegate.window?.rootViewController = mainVcIntial
        }else if(indexPath.row == 9){
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HelpDeskVC")
             appDelegate.window?.rootViewController = mainVcIntial
        }else if (indexPath.row == 17){
            let message = "Get All your GST Queries Answered with MP GST APP.Download the app from Google Play Store now"
            if let link = NSURL(string: "http://goo.gl/CFmrzD")
            {
                let objectsToShare = [message,link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                 self.present(activityVC, animated: true, completion: nil)
            }
        }else if(indexPath.row == 16){
            strTagADD = "About App"
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "About_Disclaimber_DeveloperVC")
             appDelegate.window?.rootViewController = mainVcIntial
        }else if(indexPath.row == 15){
            strTagADD = "Disclaimber"
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "About_Disclaimber_DeveloperVC")
             appDelegate.window?.rootViewController = mainVcIntial
        }else if(indexPath.row == 19){
            strTagADD = "Developer Info"
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "About_Disclaimber_DeveloperVC")
             appDelegate.window?.rootViewController = mainVcIntial
        }else if(indexPath.row == 14){
            //let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "NotificationVC")
            // appDelegate.window?.rootViewController = mainVcIntial
             showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Comming Soon...", viewcontrol: self)
        }else if(indexPath.row == 13){
//            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "FavouritesVC")
//            appDelegate.window?.rootViewController = mainVcIntial
            
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Comming Soon...", viewcontrol: self)

        }else {
            if indexPath.row == 10 {
                staticViewCome = 9
            }else if (indexPath.row == 11){
                staticViewCome = 10
            }else if (indexPath.row == 12){
                staticViewCome = 11
            }else if (indexPath.row == 18){
                staticViewCome = 12
            }else{
                staticViewCome = indexPath.row
            }
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "TutorialVideoVC")
              appDelegate.window?.rootViewController = mainVcIntial
        }
    }


   }






