//
//  ViewController.swift
//  MPGST
//
//  Created by Akhilesh Patidar on 8/10/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import  GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit

class WelcomeVC: UIViewController ,GIDSignInDelegate,GIDSignInUIDelegate{
    
    //MARK:
    //MARK: IBOutlet
    @IBOutlet weak var pagerController: UIPageControl!
    @IBOutlet weak var collectionView_Welcome: UICollectionView!
    
    //MARK:
    //MARK: CustomeVariable
    let  ary_CollectionData = [["title":"Goods And Services Tax ","subtitle" :" Good and services Tax (GST) is an indirect taxation in india merging most of the existing into single system of taxation.\nIt was intoduced as the Constitution (One Hundred and First Amendment) Act 2016, following the passage of Constitution 122nd Amendment Bill.\nThe GST is governed by GST Council and its Chairman is Union Finance Minister of India."]]
    
    
    //MARK:
    //MARK: DEFAULT FUNCTION
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        let collectionViewLayout = (self.collectionView_Welcome.collectionViewLayout as! UICollectionViewFlowLayout)
        collectionViewLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:
    //MARK: @IBAction
    @IBAction func actionFaceBook(_ sender: UIButton) {
        if checkInternetConnection(){
            FBSDKLoginManager().logOut()
            if (FBSDKAccessToken.current() != nil)
            {
              //  print(FBSDKAccessToken.current())
                // User is already logged in, do work such as go to next view controller.
            }else{
                
                let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
                fbLoginManager.logIn(withReadPermissions: ["public_profile","email"]) { (result, error) -> Void in
                    if error != nil {
                        print(error?.localizedDescription ?? 0)
                    } else if (result?.isCancelled)! {
                        print("Cancelled")
                    } else {
                        self.configureFacebook()
                    }
                }
            }
        }else{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please check internet connection", viewcontrol: self)
        }
    }
    @IBAction func actionGoogle(_ sender: Any) {
        if checkInternetConnection(){
            GIDSignIn.sharedInstance().signIn()
        }else{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please check internet connection", viewcontrol: self)
        }
        
    }
    
    
    //MARK:
    //MARK: OTHER FUNCTION
    
    
    // MARK: - GOOGLE
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        var activityIndicator = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            //let idToken = user.authentication.idToken // Safe to send to the server
           // let fullName = user.profile.name
           // let givenName = user.profile.givenName
           // let familyName = user.profile.familyName
            let email = user.profile.email
            var imgurl1 = String()
           // print("fullName:\(fullName)")
           // print("familyName:\(familyName)")
            //print("userId:\(userId)")
            //print("Email:\(email)")
            if user.profile.hasImage{
                let imgurl = user.profile.imageURL(withDimension: 100) as NSURL
                print(imgurl)
                imgurl1 =  imgurl.absoluteString!
            }
            nsud.set("YES", forKey: "loginstatus")
            nsud.set(email, forKey: "usermail")
            nsud.set(userId!, forKey: "userid")
            nsud.synchronize()
            DispatchQueue.main.async {
                let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                appDelegate.window?.rootViewController = mainVcIntial
            }
            
        } else {
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Registration failed!!!", viewcontrol: self)
            print("\(error.localizedDescription)")
        }
    }
    func configureFacebook() {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name,last_name,email,picture,link"]).start(completionHandler: { (connection,
            result, error) -> Void in
            print(result ?? 0)
            if (error == nil){
                let fbDetails = result as! NSDictionary
                let email = fbDetails["email"] as?String
                let fbid = fbDetails["id"] as! String
                let first_name = fbDetails["first_name"] as! String
                let last_name = fbDetails["last_name"] as! String
                let FullName = "\(first_name) \(last_name)"
                let picture = fbDetails["picture"] as! NSDictionary
                let data = picture["data"] as! NSDictionary
                let imageURL =  data["url"] as! String
                if let url = NSURL(string: imageURL) {
                    if let data = NSData(contentsOf: url as URL) {
                    }
                }
                if email != nil{
                    
                    nsud.set("YES", forKey: "loginstatus")
                    nsud.set(email, forKey: "usermail")
                    nsud.set(fbid, forKey: "userid")
                    nsud.synchronize()
                    DispatchQueue.main.async {
                        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }
                   
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Your A/c Not Public!!!", viewcontrol: self)

                }
            }
        })
    }
    
    
}
extension WelcomeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCell", for: indexPath as IndexPath) as! homeCollectionCell
        let dict = ary_CollectionData[0]as NSDictionary
        cell.lblWelcomeTitle.text = dict["title"]as? String
        cell.txtWelcomeDiscription.text = dict["subtitle"]as? String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView_Welcome.frame.size.width - 10 ) , height:(self.collectionView_Welcome.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collectionView_Welcome.contentOffset.x
        let w = self.collectionView_Welcome.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pagerController.currentPage = (currentPage)
    }
}

