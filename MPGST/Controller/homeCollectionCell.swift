//
//  homeCollectionCell.swift
//  MPGST
//
//  Created by Akhilesh Patidar on 8/10/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class homeCollectionCell: UICollectionViewCell {
    
    //Home Screen
    @IBOutlet weak var lbl_HomeTitle: UILabel!
    @IBOutlet weak var lbl_HomeImage: UIImageView!
    // WelcomeScreen
    @IBOutlet weak var lblWelcomeTitle: UILabel!
    @IBOutlet weak var txtWelcomeDiscription: UITextView!
}
