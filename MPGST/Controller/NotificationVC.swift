//
//  NotificationVC.swift
//  MPGST
//
//  Created by Navin on 8/13/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    var aryTvData = NSMutableArray()
    @IBOutlet weak var tvForNotification: UITableView!
    
    var strViewComeFrom : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        if strViewComeFrom != "Home" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK:- CallHelpDesk API
    func callHelpDeskAPI(strMessage :String , strurl :String , strResponce: String , strResult :String)  {
        KRProgressHUD.show(message: "Loading...")
        WebService.callSOAP_APIBY_GETHELP(soapMessage: strMessage, url: strurl, resultype: strResult, responcetype: strResponce) { (responce, status) in
            KRProgressHUD.dismiss()
            if status == "Suceess"{
                let dictTemp = responce.value(forKey: "data")as! NSDictionary
                let aryTemp = dictTemp.value(forKey: "HelpDeskDivision")as! NSArray
                self.aryTvData = NSMutableArray()
                self.aryTvData = aryTemp.mutableCopy()as! NSMutableArray
                self.tvForNotification.reloadData()
            }else{
                
                
            }
        }
    }
}
