//
//  HeplDeskDetailVC.swift
//  MPGST
//
//  Created by Navin on 8/13/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class HeplDeskDetailVC: UIViewController {
  
    var aryTvData : NSMutableArray!
    var dictData : NSMutableDictionary!
    @IBOutlet weak var tvForHelp: UITableView!
    @IBOutlet weak var lblTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
       // tvForHelp.estimatedRowHeight = 194.0
   
        lblTitle.text = dictData.value(forKey: "Division")as? String
        aryTvData = (dictData.value(forKey: "HelpDesk")as! NSArray).mutableCopy()as! NSMutableArray
        tvForHelp.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }

}
//MARK:- UITableView Delegate Methods
//MARK:-
extension  HeplDeskDetailVC  : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return aryTvData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvForHelp.dequeueReusableCell(withIdentifier: "helpdetailCell", for: indexPath as IndexPath) as! CommonTVCell
        let dictemp  = aryTvData.object(at: indexPath.row)as! NSDictionary
        
        let strName = dictemp.value(forKey: "Name")as? String
        let strMobileNumber = dictemp.value(forKey: "MobileNo")as? String
        let strCircle = dictemp.value(forKey: "Circle")as? String
        let strDesignation = dictemp.value(forKey: "Designation")as? String
        let strDepartmentNumber = dictemp.value(forKey: "DepartmentMobileNo")as? String
        cell.lblDetail_name.text = strName!
        cell.lblDetail_Designation.text = strDesignation!
        cell.lblDeail_circle.text = strCircle!
        cell.btnDetailDMobile.setTitle(strDepartmentNumber, for: .normal)
        cell.btnDetail_Mobile.setTitle(strMobileNumber, for: .normal)
        if strDepartmentNumber?.characters.count == 0 {
            cell.btnDetailDMobile.isHidden = true
            cell.lblDetail_StaticDmobile.isHidden = true
        }else{
            cell.btnDetailDMobile.isHidden = false
            cell.lblDetail_StaticDmobile.isHidden = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dictemp  = aryTvData.object(at: indexPath.row)as! NSDictionary
        let strDepartmentNumber = dictemp.value(forKey: "DepartmentMobileNo")as? String
        if strDepartmentNumber?.characters.count == 0 {
            return 151
        }else{
         return 195
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
