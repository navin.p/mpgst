//
//  faq_Rules_ScheduleDetailVC.swift
//  MPGST
//
//  Created by Navin Patidar on 8/17/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class faq_Rules_ScheduleDetailVC: UIViewController {
    
    var strCome : String!
    var dictData = NSMutableDictionary()
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnTransP: UIButton!
    @IBOutlet weak var viewComment: CardView!
    @IBOutlet weak var txtComment: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictData)
        btnLike.isHidden = false
        if strCome == "FAQ" {
            lblTitle.text = "SmartFAQ"
            let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            setLikeUnlikeButtonImage(status: IsFavourite!)
        }else if(strCome == "RULES"){
            lblTitle.text = "Rules"
            //let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
           // setLikeUnlikeButtonImage(status: IsFavourite!)
            btnLike.isHidden = true
        }else if(strCome == "Schedules"){
            lblTitle.text = "Schedules"
            btnLike.isHidden = true
            //let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            //setLikeUnlikeButtonImage(status: IsFavourite!)
        }else if(strCome == "ACT"){
            lblTitle.text = dictData.value(forKey: "Title")as? String
            let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            setLikeUnlikeButtonImage(status: IsFavourite!)
        }else if(strCome == "Article"){
            lblTitle.text = "Articles"
            btnComment.isHidden = false
            let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            setLikeUnlikeButtonImage(status: IsFavourite!)
            
        }else if (strCome == "ExpertContribution"){
            lblTitle.text = "Expert Contribution"
            let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            setLikeUnlikeButtonImage(status: IsFavourite!)
        }
        tv.dataSource = self
        tv.delegate = self
        tv.estimatedRowHeight = 162.0
        self.viewComment.isHidden = true
        self.btnTransP.isHidden = true
        txtComment.layer.borderColor = hexStringToUIColor(hex: "2F5DD1").cgColor
        txtComment.layer.borderWidth = 1.0
        txtComment.text = "Write Comment here"
        txtComment.textColor = UIColor.lightGray
        txtComment.returnKeyType = .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    //MARK:- @IBAction
    //MARK:-
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnComment(_ sender: UIButton) {
        setView(view: self.viewComment, hidden: false)
        
    }
    
 
    @IBAction func actionOnCommentSend(_ sender: UIButton) {
        if sender == btnTransP {
            setView(view: self.viewComment, hidden: true)
        }else{
            if (txtComment.text.characters.count != 0 && txtComment.text != "Write Comment here") {
                if checkInternetConnection() == true {
                    let strUserid = nsud.value(forKey: "userid")as! String
                    print(strUserid)
                    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddArticleComment xmlns='http://gst.citizencop.org/'><Id>\(strUserid)</Id><Comment>\(txtComment.text!)</Comment></AddArticleComment></soap12:Body></soap12:Envelope>"
                    
                    KRProgressHUD.show(message: "Loading...")
                    WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddArticleCommentResult", responcetype: "AddArticleCommentResponse") { (responce, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = responce.value(forKey: "data")as! NSDictionary
                            print(dictTemp)
                            if dictTemp.count != 0{
                                
                            }else{
                                
                                
                            }
                            self.setView(view: self.viewComment, hidden: true)
                            
                        }else{
                            self.setView(view: self.viewComment, hidden: true)
                        }
                    }
                }else{
                    setView(view: self.viewComment, hidden: true)
                }
            }else{
                setView(view: self.viewComment, hidden: true)
            }
        }
    }
    
    @IBAction func actionShare(_ sender: UIButton) {
        
        if strCome == "FAQ" {
           
        }else if(strCome == "RULES"){
           
        }else if(strCome == "Schedules"){
           
        }else if(strCome == "ACT"){
            
        }else if(strCome == "Article"){
            
        }else if (strCome == "ExpertContribution"){
            
            
        }
       // showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Comming Soon...", viewcontrol: self)
    }
    @IBAction func actionLike(_ sender: UIButton) {
        if checkInternetConnection() == true {
            
            //-------For FAQ Detail ---------//
            if (strCome == "FAQ"){
                let strItemID = self.dictData.value(forKey: "Smart_FAQ_Id") as? Int
                callLikeUnlikeAPI(strItemID: "\(strItemID!)", strFavouriteType: "SmartFAQ")
            }
            
        //-------For ACT Detail ---------//
                
            else if (strCome == "ACT"){
                let strItemID = self.dictData.value(forKey: "GSTLawId") as? Int
                callLikeUnlikeAPI(strItemID: "\(strItemID!)", strFavouriteType: "GSTModalLaw")
            }

        //-------For Article Detail ---------//
                
            else if (strCome == "Article"){
                let strItemID = self.dictData.value(forKey: "Id") as? Int
                callLikeUnlikeAPI(strItemID: "\(strItemID!)", strFavouriteType: "GSTModalLaw")
            }
            
            //-------For Expert Contribution Detail ---------//
            
            else if (strCome == "ExpertContribution"){
                let strItemID = self.dictData.value(forKey: "Knowledge_Id") as? Int
                callLikeUnlikeAPI(strItemID: "\(strItemID!)", strFavouriteType: "ShareYourKnowledge")
            }
            
        }else{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please check internet connection", viewcontrol: self)
        }
    }
    
    
    //MARK:- @Other function
    //MARK:-
    
    func checkItemIsFavouriteOrNot(strItemID : String , strItemType : String, strEmail : String) {
        DispatchQueue.main.async {
            print(strItemID)
            print(strItemType)
            print(strEmail)
            KRProgressHUD.show(message: "Loading...")
            let soapmsg = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFavouriteFeature xmlns='http://gst.citizencop.org/'><FavouriteTypeId>\(strItemID)</FavouriteTypeId><FavouriteType>\(strItemType)</FavouriteType><EmailId>\(nsud.value(forKey: "usermail")as!String)</EmailId></GetFavouriteFeature></soap12:Body></soap12:Envelope>"
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapmsg, url: BaseURL, resultype: "GetFavouriteFeatureResult", responcetype: "GetFavouriteFeatureResponse", OnResultBlock: { (dictData, status) in
                KRProgressHUD.dismiss()
                print(dictData)
                if status == "Suceess"{
                    let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                    if dictTemp.count > 0{
                        if dictTemp.value(forKey: "Result")as! String  == "True"{
                            self.btnLike.isUserInteractionEnabled = false
                            self.btnLike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
                        }else{
                            self.btnLike.isUserInteractionEnabled = true
                            self.btnLike.setImage(UIImage(named:"favorite_icon"), for: .normal)
                        }
                    }
                }else{
                    
                }
            })
        }
    }
    
    func callLikeUnlikeAPI(strItemID : String ,strFavouriteType : String )  {
        print(strItemID);
        print(strFavouriteType);
        
        KRProgressHUD.show(message: "Loading...")
        var soapMessage = String()
        if strCome == "FAQ" {
            soapMessage  = creatSoapMessage(itemid: strItemID, type: "SmartFAQ")
        }else if (strCome == "Article"){
            soapMessage  = creatSoapMessage(itemid: strItemID, type: "Article")
        }else if (strCome == "ACT"){
            soapMessage  = creatSoapMessage(itemid: strItemID, type: "GSTModalLaw")
        }else if (strCome == "ExpertContribution"){
            soapMessage  = creatSoapMessage(itemid: strItemID, type: "ShareYourKnowledge")
        }


        
        WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddFavouriteFeatureResult", responcetype: "AddFavouriteFeatureResponse", OnResultBlock: { (dictData, status) in
            KRProgressHUD.dismiss()
            print(dictData)
            if status == "Suceess"{
                let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                if dictTemp.count > 0{
                    if dictTemp.value(forKey: "Result")as! String  == "True"{
                        self.btnLike.isUserInteractionEnabled = false
                        self.btnLike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
                    }
                }
                
                
            }else{
                
            }
        })
    }
    
    func creatSoapMessage(itemid : String , type : String ) -> String {
        let strEmail = nsud.value(forKey: "usermail")as!String
        print(itemid)
        print(strEmail)
        print(type)
        
        
        return  "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddFavouriteFeature xmlns='http://gst.citizencop.org/'><FavouriteTypeId>\(itemid)</FavouriteTypeId><IsFavourite>true</IsFavourite><FavouriteType>\(type)</FavouriteType><EmailId>\(strEmail)</EmailId></AddFavouriteFeature></soap12:Body></soap12:Envelope>"
    }
    
    
    
    func setLikeUnlikeButtonImage(status : Bool, strcome: String) {
        if status == true {
            btnLike.isUserInteractionEnabled = false
            btnLike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
        }else{
            btnLike.isUserInteractionEnabled = true
            btnLike.setImage(UIImage(named:"favorite_icon"), for: .normal)
            
            if checkInternetConnection() == true {
                
                //ForCheckViedoAPI
                if strcome == "FAQ" {
                    let strItemID = self.dictData.value(forKey: "Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "SmartFAQ", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                        //For ACT detail
               
                else if(strcome == "ACT"){
                    let strItemID = self.dictData.value(forKey: "GSTLawId") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "GSTModalLaw", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                //For Article detail
                else if(strcome == "Article"){
                    let strItemID = self.dictData.value(forKey: "Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "Article", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                //For ExpertContribution detail
                else if(strcome == "ExpertContribution"){
                    let strItemID = self.dictData.value(forKey: "Knowledge_Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "ShareYourKnowledge", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
            }else{
                
            }
        }
    }

    
    func setView(view: UIView, hidden: Bool) {
        txtComment.text = "Write Comment here"
        txtComment.textColor = UIColor.lightGray
        view.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            view.transform = CGAffineTransform(scaleX: 1,y: 1)
            self.btnTransP.isHidden = hidden
            view.isHidden = hidden
        })
    }
    
    
    func setLikeUnlikeButtonImage(status : Bool) {
        
        if status == true {
            btnLike.isUserInteractionEnabled = false
            btnLike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
        }else{
            btnLike.isUserInteractionEnabled = true
            btnLike.setImage(UIImage(named:"favorite_icon"), for: .normal)
            
            if checkInternetConnection() == true {
                
        //-------For Check FAQ Detail-------//
                
                if strCome == "FAQ" {
                    let strItemID = self.dictData.value(forKey: "Smart_FAQ_Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "SmartFAQ", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                
        //-------For Check GSTModalLaw Detail-------//
                if strCome == "ACT" {
                    let strItemID = self.dictData.value(forKey: "GSTLawId") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "GSTModalLaw", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                
        //-------For Check Article Detail-------//
                if strCome == "Article" {
                    let strItemID = self.dictData.value(forKey: "Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "Article", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }

                
               
        //-------For Check ExpertContribution Detail-------//
                    
                else if(strCome == "ExpertContribution"){
                    let strItemID = self.dictData.value(forKey: "Knowledge_Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "ShareYourKnowledge", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                
            }else{
                
            }
        }

        
    }
}
//MARK:- UITableView Delegate Methods
//MARK:-
extension  faq_Rules_ScheduleDetailVC  : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //------------------ForFAQ-------------------//
        if strCome == "FAQ" {
            let cell = tv.dequeueReusableCell(withIdentifier: "FaqDetailCell", for: indexPath as IndexPath) as! CommonTVCell
            let strTitle = self.dictData.value(forKey: "Question") as? String
            let strSubTitle = self.dictData.value(forKey: "Answer") as? String
            let strDate = self.dictData.value(forKey: "Created_Date") as? String
            cell.img_FaqImage.image = UIImage(named:"faq_icon")
            cell.lbl_FAQDetail_Title.text = strTitle
            cell.txt_FAQDetail.attributedText = stringFromHtml(string: strSubTitle!)
            cell.lbl_FAQDetail_Date.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'hh:mm:ss")
            return cell
        }
            //------------------ForACT-------------------//
            
        else if strCome == "ACT" {
            let cell = tv.dequeueReusableCell(withIdentifier: "FaqDetailCell", for: indexPath as IndexPath) as! CommonTVCell
            let strTitle = self.dictData.value(forKey: "Title") as? String
            let strSubTitle = self.dictData.value(forKey: "Description") as? String
            let strDate = self.dictData.value(forKey: "CreatedDate") as? String
            
            cell.lbl_FAQDetail_Title.text = strTitle
            cell.txt_FAQDetail.attributedText = stringFromHtml(string: strSubTitle!)
            cell.lbl_FAQDetail_Date.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'hh:mm:ss.SSS")
            cell.img_FaqImage.image = UIImage(named:"law_icon")
            
            return cell
        }
            //------------------ForARTICLE-------------------//
            
        else if strCome == "Article" {
            let cell = tv.dequeueReusableCell(withIdentifier: "RulesDetailDetailCell", for: indexPath as IndexPath) as! CommonTVCell
            let strTitle = self.dictData.value(forKey: "Title") as? String
            let strSubTitle = self.dictData.value(forKey: "Description") as? String
            let strDate = self.dictData.value(forKey: "CreatedDate") as? String
            cell.lbl_RulesSchedule_Title.text = strTitle
            cell.txt_RulesSchedule.attributedText = stringFromHtml(string: strSubTitle!)
            cell.lbl_RulesSchedule_Date.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'hh:mm:ss")
            return cell
        }
            
            //------------------Expert Contribution-------------------//
            
        else {
            let cell = tv.dequeueReusableCell(withIdentifier: "RulesDetailDetailCell", for: indexPath as IndexPath) as! CommonTVCell
            let strTitle = self.dictData.value(forKey: "Title") as? String
            let strSubTitle = self.dictData.value(forKey: "Description") as? String
            var strDate = String()
            if lblTitle.text == "Expert Contribution" {
                strDate = (self.dictData.value(forKey: "Created_Date") as? String)!
            }else{
                strDate = (self.dictData.value(forKey: "CreatedDate") as? String)!
            }
            cell.lbl_RulesSchedule_Title.text = strTitle
            cell.txt_RulesSchedule.attributedText = stringFromHtml(string: strSubTitle!)
            cell.lbl_RulesSchedule_Date.text = dateTimeConvertor(str: strDate, formet: "yyyy-MM-dd'T'hh:mm:ss")
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tv.frame.size.height
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
extension faq_Rules_ScheduleDetailVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtComment {
            if (txtComment?.text == "Write Comment here") {
                txtComment!.text = nil
                txtComment!.textColor = UIColor.black
            }
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtComment {
            let strDesc = txtComment?.text.trimmingCharacters(in: .whitespacesAndNewlines)
            if strDesc == "" {
                txtComment?.text = "Write Comment here"
                txtComment?.textColor = UIColor.lightGray
            } else {
                txtComment?.textColor = UIColor.black
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentCharacterCount = textView.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        if textView == txtComment {
            if (text == "\n") {
                self.view.endEditing(true)
                return false
            }
            let strDesc = txtComment.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if strDesc != "" {
            } else {
                txtComment?.text = ""
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 200
            
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
