//
//  HelpDeskVC.swift
//  MPGST
//
//  Created by Navin on 8/13/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class HelpDeskVC: UIViewController {
    
    var strViewComeFrom : String!
    var aryTvData = NSMutableArray()
    var dictData : NSMutableDictionary!
    
    //For check Act Data Come From
    var strActScreenStatus : String!
    
    
    @IBOutlet weak var tvForHelp: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(strViewComeFrom)
        if strViewComeFrom == nil {
            strViewComeFrom = "Help"
        }
            if strViewComeFrom == "ACT" {
                print(dictData)
                if strActScreenStatus != nil {
                  aryTvData.add(dictData)
                lblTitle.text = dictData?.value(forKey: "Title")as? String

                }else{
                    aryTvData = (dictData.value(forKey: "GSTLawChapter")as! NSArray).mutableCopy()as! NSMutableArray
                    let dictTemp = aryTvData.object(at: 0)as? NSDictionary
                    lblTitle.text = dictTemp?.value(forKey: "Title")as? String

                }
                tvForHelp.reloadData()
            }else{
                setDataOnHelpDesk()
            }
        tvForHelp.estimatedRowHeight = 71.0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionBack(_ sender: UIButton) {
        
        if strViewComeFrom == "ACT"{
            self.navigationController?.popViewController(animated: true)
            
        }else{
            if strViewComeFrom != "Home" {
                let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                appDelegate.window?.rootViewController = mainVcIntial
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    //MARK:- CallHelpDesk API
    
    func setDataOnHelpDesk() {
        lblTitle.text = "Help Desk"
        if checkInternetConnection() == true {
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetHelpDesk xmlns='http://gst.citizencop.org/'></GetHelpDesk></soap12:Body></soap12:Envelope>"
            
            callHelpDeskAPI(strMessage: soapMessage, strurl: BaseURL, strResponce: "GetHelpDeskResponse", strResult: "GetHelpDeskResult")
        }else{
            let alert = UIAlertController(title: "Alert!", message: "Please check internet connection", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                if self.strViewComeFrom != "Home" {
                    let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func callHelpDeskAPI(strMessage :String , strurl :String , strResponce: String , strResult :String)  {
        KRProgressHUD.show(message: "Loading...")
        WebService.callSOAP_APIBY_GETHELP(soapMessage: strMessage, url: strurl, resultype: strResult, responcetype: strResponce) { (responce, status) in
            KRProgressHUD.dismiss()
            if status == "Suceess"{
                let dictTemp = responce.value(forKey: "data")as! NSDictionary
                // print(dictTemp)
                let aryTemp = dictTemp.value(forKey: "HelpDeskDivision")as! NSArray
                self.aryTvData = NSMutableArray()
                self.aryTvData = aryTemp.mutableCopy()as! NSMutableArray
                self.tvForHelp.reloadData()
            }else{
                
                
            }
        }
    }
    
    
    //MARK:-
}

//MARK:- UITableView Delegate Methods
//MARK:-
extension  HelpDeskVC  : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTvData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForHelp.dequeueReusableCell(withIdentifier: "helpdeskCell", for: indexPath as IndexPath) as! CommonTVCell
        let dictTemp = aryTvData.object(at: indexPath.row)as? NSDictionary
        var strTitle = String()
        if strViewComeFrom == "ACT" {
            strTitle = (dictTemp?.value(forKey: "SubTitle")as? String)!
            cell.lbl_Act_HelpTitle.text = strTitle
            
        }else{
            strTitle = (dictTemp?.value(forKey: "Division")as? String)!
            cell.lbl_Act_HelpTitle.text = strTitle
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if strViewComeFrom == "ACT" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
            vc.strCome = "ACT"
            let dictTemp = aryTvData.object(at: indexPath.row)as? NSDictionary
            vc.dictData = dictTemp?.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HeplDeskDetailVC") as! HeplDeskDetailVC
            vc.dictData = (aryTvData.object(at: indexPath.row)as? NSDictionary)?.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
