//
//  About_Disclaimber_DeveloperVC.swift
//  MPGST
//
//  Created by Navin on 8/15/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class About_Disclaimber_DeveloperVC: UIViewController {

    var strViewComeFrom : String!
    var dictData = NSDictionary()
    
    @IBOutlet weak var webForD: UIWebView!
    @IBOutlet weak var lblHeader: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
         lblHeader.text = strTagADD
         print(dictData)
        var strURL = String()
        if checkInternetConnection() == true {
            webForD.delegate = self
            KRProgressHUD.show(message: "Loading...")
            if strTagADD == "About App" {
                strURL = "http://gst.citizencop.org/AboutApp.aspx"
            }else if(strTagADD == "Disclaimber"){
                strURL = "http://gst.citizencop.org/Disclaimer.aspx"
            }else if (strTagADD == "SGST Notification"){
                  strURL = dictData.value(forKey: "NotificationLink")as! String
                 strURL = "http://gst.citizencop.org/galleryimages/\(strURL)"
            }
            else{
                strURL = "http://gst.citizencop.org/DeveloperInfo.aspx"
            }
            if let url = URL(string: strURL) {
                let request = URLRequest(url: url)
                webForD.loadRequest(request)
            }
        }else{
        
            let alert = UIAlertController(title: "Alert!", message: "Please check internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                if self.strViewComeFrom != "Home" {
                    let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        if strViewComeFrom != "Home" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension About_Disclaimber_DeveloperVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        KRProgressHUD.dismiss()
    }
}
