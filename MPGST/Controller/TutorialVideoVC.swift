//
//  TutorialVideoVC.swift
//  MPGST
//
//  Created by Akhilesh Patidar on 8/11/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class TutorialVideoVC: UIViewController ,UIScrollViewDelegate{
    
    //MARK:
    //MARK: @IBOutlet
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var scrollContain: UIScrollView!
    @IBOutlet weak var txtSearch: UITextField!
    
    //View1ForVedio
    @IBOutlet weak var tvForVideo: UITableView!
    
    //View1ForFAQ
    @IBOutlet weak var tvForFAQ: UITableView!
    
    //View For ACTS
    @IBOutlet weak var tvForACTS: UITableView!
    
    //View For Rules
    @IBOutlet weak var tvForRULES: UITableView!
    
    //View For Schedule
    @IBOutlet weak var tvForSCHEDULE: UITableView!
    
    // ASK OUR EXPERT
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnSubmitButtomConstraint: NSLayoutConstraint!
    
    //View For Expert Combination
    @IBOutlet weak var tvExpertCombination: UITableView!
    
    //View For Article
    @IBOutlet weak var tvArticles: UITableView!
    
    // View  News
    @IBOutlet weak var tvForNews: UITableView!

    // View  SGST
    @IBOutlet weak var tvForSgstNotification: UITableView!
    
    // View RATES
    @IBOutlet weak var tvForRates: UITableView!
    @IBOutlet weak var btnGood: UIButton!
    @IBOutlet weak var btnServices: UIButton!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblServices: UILabel!

    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblHsnCode: UILabel!
    @IBOutlet weak var txtItemname: UITextView!
    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var viewtrans: UIView!
    @IBOutlet weak var viewRatesDetail: UIView!

    
    //View Calculater
    @IBOutlet weak var txtCalculatorDiscription: UITextView!
    @IBOutlet weak var txtCalculatorCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCalculatorUnit: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCalculatorQuantity: SkyFloatingLabelTextField!
    @IBOutlet weak var lblCalculatorrate: UILabel!
    @IBOutlet weak var tvDropDown: UITableView!
    
    
    @IBOutlet weak var viewCalanderDetail: CardView!
    @IBOutlet weak var viewCalculatedata: UIView!
    
    @IBOutlet weak var txtCalculatorDetail: UITextView!
    @IBOutlet weak var lblCalculatorCostValue: UILabel!
    @IBOutlet weak var txtCalculatorGstValue: UILabel!
    @IBOutlet weak var txtCalculatorTotalValue: UILabel!
    //MARK:
    //MARK: @VARIABLE
    var array_btnMenu = NSMutableArray()
    var array_TV = NSMutableArray()
    var strComeFrom = String()
    var strButtonPossition : Int!
    var strViewComeFrom : String!
    var array_DropDownTV = NSMutableArray()

  
    
    
    //MARK:
    //MARK: DEFAULT FUNCTION
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollContain.delegate = self
        ///print(staticViewCome)
        array_btnMenu = ["TUTORIAL VIDEO", "SMART FAQ", "ACTS", "RULES","SCHEDULES" ,"SGST NOTIFICATIONS" ,"GST RATES","GST CALCULATOR" ,"NEWS UPDATES" ,"ARTICLES" ,"ASK OUR EXPERT","EXPERT CONTRIBUTION"]
        
        //  call_DashBoardData_API()
        staticViewCome =   staticViewCome - 1
        print(staticViewCome)
        // Ask OUR EXPERT
        txtEmail.text = "  \(nsud.value(forKey: "usermail")as! String)"
        txtEmail.delegate = self
       
        txtDescription.delegate = self
        txtDescription.text = "Enter question here"
        txtDescription.textColor = UIColor.lightGray
       
        //View For Expert Combination & Article
        tvArticles.estimatedRowHeight = 150.0
        tvExpertCombination.estimatedRowHeight = 150.0
        tvForRates.estimatedRowHeight = 142
        self.tvForRates.tag = 10
        
        //View For Rates detail

        self.viewtrans.isHidden = true
        self.viewRatesDetail.isHidden = true
        
        //View for Calculater
        txtCalculatorDiscription.delegate = self
        txtCalculatorDiscription.text = "Description"
        txtCalculatorDiscription.textColor = UIColor.lightGray
        txtCalculatorDiscription.returnKeyType = .default
        viewCalanderDetail.isHidden = true
      //  viewCalculatedata.layer.borderColor = UIColor.gray as! CGColor
      //  viewCalculatedata.layer.borderWidth = 1.0
        self.txtSearch.isEnabled = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tvDropDown.isHidden = true
        setUPMenuTap()
        NotificationCenter.default.addObserver(self, selector: #selector(TutorialVideoVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TutorialVideoVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    override func viewWillLayoutSubviews() {
        let sWidth = (Int(self.scrollContain.frame.size.width) * staticViewCome)
        let point = CGPoint(x: sWidth, y: 0)
        scrollContain.contentOffset = point
    }
    override func viewDidAppear(_ animated: Bool) {
        scrollContain.contentSize = CGSize(width: scrollContain.frame.size.width*12, height: scrollContain.frame.size.height)
        NotificationCenter.default.addObserver(self, selector: #selector(TutorialVideoVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TutorialVideoVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        let info = notification.userInfo!
        var kbRect = (info[UIKeyboardFrameEndUserInfoKey]! as! NSValue).cgRectValue
        kbRect = view.convert(kbRect, from: nil)
        // bottomHeight matches keyboard height
        UIView.animate(withDuration: 0.0) {
            self.btnSubmitButtomConstraint.constant = kbRect.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        // No keyboard, so bottomHeight back to 0
        UIView.animate(withDuration: 0.0) {
            self.btnSubmitButtomConstraint.constant = 0
        }
    }
    //MARK:
    //MARK: @IBAction
    
    @IBAction func actionDoneCaculatore(_ sender: Any) {
        
        self.viewtrans.isHidden = false
        self.viewCalanderDetail.isHidden = false
        self.txtCalculatorCode .text = ""
        self.txtCalculatorUnit .text = ""
        self.txtCalculatorQuantity.text = ""
        self.txtCalculatorGstValue .text = ""
        self.lblCalculatorrate.text = "Rates"
        self.viewtrans.isHidden = true
        self.viewCalanderDetail.isHidden = true
        self.view.endEditing(true)
    }
    @IBAction func actionCalculate(_ sender: Any) {
        
        if txtCalculatorDiscription.text.characters.count == 0  || txtCalculatorDiscription.text == "Description" {
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please Select Description.", viewcontrol: self)
        }else if(txtCalculatorUnit.text?.characters.count == 0){
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please Enter unit.", viewcontrol: self)

        }else if(txtCalculatorQuantity.text?.characters.count == 0){
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please Enter Quantity.", viewcontrol: self)
 
        }else{
            txtCalculatorDetail.text = txtCalculatorDiscription.text
            let f1 = CGFloat((txtCalculatorUnit.text! as NSString).floatValue)
            let f2 = CGFloat((txtCalculatorQuantity.text! as NSString).floatValue)
           let text2 = lblCalculatorrate.text?.replacingOccurrences(of: "%", with: "", options: NSString.CompareOptions.literal, range:nil)
            let f3 = CGFloat((text2! as NSString).floatValue)
            let strGstRate = ((f1 * f2) * f3)/100
            self.lblCalculatorCostValue.text = "\(f1 * f2)"
            self.txtCalculatorGstValue.text = "\(strGstRate)"
            self.txtCalculatorTotalValue.text = "\(strGstRate + (f1 * f2))"
            self.viewtrans.isHidden = false
            self.viewCalanderDetail.isHidden = false
            self.view.endEditing(true)
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        if strViewComeFrom != "Home" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actionHidePOPUP(_ sender: Any) {
       self.viewtrans.isHidden = true
        self.viewRatesDetail.isHidden = true
    }

    
    @IBAction func actionGood(_ sender: Any) {
        lblGood.backgroundColor = hexStringToUIColor(hex: "2F5DD1")
        lblServices.backgroundColor = UIColor.clear
        
        if aryForGoodTax.count == 0 {
            var aryTemp = NSArray()
            if !(checkInternetConnection()){
                aryTemp = getDataFromLocal(strEntity: "Goods", strkey: "goodsdata")
                // print(aryTemp.count)
                if ((aryTemp.count) != 0){
                    aryForGoodTax = NSMutableArray()
                    aryForGoodTax = aryTemp.mutableCopy()as! NSMutableArray
                    array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                    self.tvForRates.isHidden = false
                    self.tvForRates.reloadData()
                }
            }
            else{
                KRProgressHUD.show(message: "Loading...")
                let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGSTNotification xmlns='http://gst.citizencop.org/'><ItemType>Goodstax</ItemType></GetGSTNotification></soap12:Body></soap12:Envelope>"
                
                WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetGstItemListResult", responcetype: "GetGstItemListResponse", OnResultBlock: { (result, status) in
                    KRProgressHUD.dismiss()
                    if status == "Suceess"{
                        let dictTemp = result.value(forKey: "data")as! NSDictionary
                        let aryTemp = dictTemp.value(forKey: "GSTNotification")as! NSArray
                        deleteAllRecords(strEntity:"Goods")
                        saveDataInLocal(strEntity: "Goods", strKey: "goodsdata", data: aryTemp)
                        aryForGoodTax = NSMutableArray()
                        aryForGoodTax = aryTemp.mutableCopy()as! NSMutableArray
                        self.array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                        self.tvForRates.isHidden = false
                        self.tvForRates.reloadData()
                    }else{
                        self.array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                        self.tvForRates.isHidden = false
                        self.tvForRates.reloadData()
                    }
                })
            }
        }
        else{
            self.array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
            self.tvForRates.isHidden = false
            self.tvForRates.reloadData()
        }
        self.tvForRates.tag = 10

    }
    @IBAction func actionServices(_ sender: Any) {
        lblGood.backgroundColor = UIColor.clear
        lblServices.backgroundColor = hexStringToUIColor(hex: "2F5DD1")
        if aryForServiceTax.count == 0 {
            var aryTemp = NSArray()
            if !(checkInternetConnection()){
                aryTemp = getDataFromLocal(strEntity: "Service", strkey: "servicedata")
                // print(aryTemp.count)
                if ((aryTemp.count) != 0){
                    aryForServiceTax = NSMutableArray()
                    aryForServiceTax = aryTemp.mutableCopy()as! NSMutableArray
                    array_TV = aryForServiceTax.mutableCopy()as! NSMutableArray
                    self.tvForRates.isHidden = false
                    self.tvForRates.reloadData()
                }
            }
            else{
                KRProgressHUD.show(message: "Loading...")
                let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGstItemList xmlns='http://gst.citizencop.org/'><ItemType>Servicestax</ItemType></GetGstItemList></soap12:Body></soap12:Envelope>"
                
               WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetGstItemListResult", responcetype: "GetGstItemListResponse", OnResultBlock: { (result, status) in
                    KRProgressHUD.dismiss()
                    if status == "Suceess"{
                        let dictTemp = result.value(forKey: "data")as! NSDictionary
                        let aryTemp = dictTemp.value(forKey: "GstItemList")as! NSArray
                        deleteAllRecords(strEntity:"Service")
                        saveDataInLocal(strEntity: "Service", strKey: "servicedata", data: aryTemp)
                        aryForServiceTax = NSMutableArray()
                        aryForServiceTax = aryTemp.mutableCopy()as! NSMutableArray
                        self.array_TV = aryForServiceTax.mutableCopy()as! NSMutableArray
                        self.tvForRates.isHidden = false
                        self.tvForRates.reloadData()
                    }else{
                        self.array_TV = aryForServiceTax.mutableCopy()as! NSMutableArray
                        self.tvForRates.isHidden = false
                        self.tvForRates.reloadData()
                    }
                })
            }
        }
        else{
            self.array_TV = aryForServiceTax.mutableCopy()as! NSMutableArray
            self.tvForRates.isHidden = false
            self.tvForRates.reloadData()
        }
     self.tvForRates.tag = 11
    }
    
    @IBAction func actionSubmitAskOurExpert(_ sender: Any) {
       
        if (txtDescription.text.characters.count > 0){
            let strUserID = nsud.value(forKey: "userid")as! String
            let strTitle = "title"
            let strQuestion = "Question"
            let strAnswer = "Answer"
            let strHindiTitle = "HindiTitle"
            let strHindiAnswer = "HindiAnswer"
            let strHindiQuestion = "HindiQuestion"
            let strEmail = nsud.value(forKey: "usermail")as! String
                KRProgressHUD.show(message: "Loading...")
                let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AskQuestionToExpert xmlns='http://gst.citizencop.org/'><UserId>\(strUserID)</UserId><Title>\(strTitle)</Title><Question>\(strQuestion)</Question><Answer>\(strAnswer)</Answer><HindiTitle>\(strHindiTitle)</HindiTitle><HindiQuestion>\(strHindiQuestion)</HindiQuestion><HindiAnswer>\(strHindiAnswer)</HindiAnswer><EmailId>\(strEmail)</EmailId></AskQuestionToExpert></soap12:Body></soap12:Envelope>"
                
                WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AskQuestionToExpertResult", responcetype: "AskQuestionToExpertResponse", OnResultBlock: { (result, status) in
                    KRProgressHUD.dismiss()
                    if status == "Suceess"{
                        
                    }else{
                       
                    }
                })
        }
    }

    //MARK:
    // MARK:-- scrollView Delegate ()--
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isEqual(scrollContain) {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                self.scrollContain.contentOffset.y = CGFloat(0)
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isEqual(scrollContain){
            let page =  Int(scrollView.contentOffset.x / scrollView.frame.size.width)
            staticViewCome  = page
            self.scrollContain.isHidden = true
            setUPMenuTap()
        }
    }
    //MARK:
    //MARK: OTHER FUNCTION
    func setUPMenuTap(){
        array_TV = NSMutableArray()
        self.scrollContain.isHidden = true
        let subViews = self.scroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        self.view.endEditing(true)
        var iWidthButton: CGFloat = 0.0
        var totalx: CGFloat = 0.0
        for i in 0..<array_btnMenu.count {
            let btn_Menu = UIButton()
            let lblIndicate = UILabel()
            let strTitle = array_btnMenu[i]as! String
            btn_Menu.setTitle(strTitle as String?, for: .normal)
            btn_Menu.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
            btn_Menu.setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
            let maxLabelSize: CGSize = CGSize(width: 300,height: CGFloat(9999))
            let contentNSString = strTitle as NSString
            let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)], context: nil)
            iWidthButton = expectedLabelSize.size.width + 40
            btn_Menu.frame = CGRect(x: totalx, y: 0, width: iWidthButton - 1, height:scroll.frame.size.height)
            lblIndicate.frame = CGRect(x: totalx, y: scroll.frame.size.height - 2  , width: iWidthButton - 1, height:scroll.frame.size.height)
            totalx = totalx + iWidthButton
            self.scroll.layer.cornerRadius = 0.0
            btn_Menu.tag = i
            lblIndicate.tag = i
            btn_Menu.addTarget(self, action: #selector(self.action_ChangeMenu), for: .touchUpInside)
            scroll.addSubview(btn_Menu)
            scroll.addSubview(lblIndicate)
            if i == staticViewCome {
                btn_Menu.setTitleColor(UIColor.black, for: UIControlState.normal)
                lblIndicate.backgroundColor = hexStringToUIColor(hex: "26336B")
                strButtonPossition =  Int (btn_Menu.frame.origin.x)
            }
            else {
                btn_Menu.setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
                lblIndicate.backgroundColor = UIColor.white
            }
        }
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scroll.contentSize  = CGSize(width: totalx, height: self.scroll.frame.size.height)
             
                    if self.strButtonPossition == 0{
                        self.scroll.contentOffset.x = CGFloat(self.strButtonPossition)
                    }else{
                        self.scroll.contentOffset.x = CGFloat(self.strButtonPossition - 100)
                    }
                }, completion: nil)
        
            self.view.layoutIfNeeded()
            self.viewWillLayoutSubviews()
            self.tvForVideo.isHidden = true
            self.tvForFAQ.isHidden = true
            self.tvForACTS.isHidden = true
            self.tvForRULES.isHidden = true
            self.tvForSCHEDULE.isHidden = true
            self.tvArticles.isHidden = true
            self.tvForNews.isHidden = true
            self.tvExpertCombination.isHidden = true
            self.tvForRates.isHidden = true
            self.setUpTableData()
       }
    func action_ChangeMenu(sender: AnyObject) {
        let subviews : NSArray = self.scroll.subviews as NSArray
        for btn_Menu in subviews{
            if (btn_Menu is UIButton) {
                if (btn_Menu as! UIButton).tag == sender.tag {
                    (btn_Menu as AnyObject).setTitleColor(UIColor.black, for: UIControlState.normal)
                    
                    staticViewCome = sender.tag
                    let sWidth = (Int(self.scrollContain.frame.size.width) * staticViewCome )
                    self.scrollContain.contentOffset.x = CGFloat(sWidth)
                    setUpTableData()
                }
                else {
                    (btn_Menu as AnyObject).setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
                }
            }else if(btn_Menu is UILabel){
                if (btn_Menu as! UILabel).tag == sender.tag {
                    (btn_Menu as! UILabel).backgroundColor = hexStringToUIColor(hex: "26336B")
                }
                else {
                    (btn_Menu as! UILabel).backgroundColor  = UIColor.white
                }
            }
        }
    }
    func creatSoapMessage(str : String) -> String {
        return  "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllFeature_IsFavouriteByFeatureType xmlns='http://gst.citizencop.org/'><Language>English</Language><FeatureType>\(str)</FeatureType><EmailId>\(nsud.value(forKey: "usermail")as!String)</EmailId></GetAllFeature_IsFavouriteByFeatureType></soap12:Body></soap12:Envelope>"
    }
       
    
    //MARK:
    //MARK: WEBSERVICES CALLING
    
    func setUpTableData(){
        self.txtSearch.isEnabled = true
        array_TV = NSMutableArray()
        var aryTemp = NSArray()
        //------------ For Video Tutorial-------------
        if staticViewCome == 0 {
            if array_TutorialVideo.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Videotutorial", strkey: "video")
                    //print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        array_TutorialVideo = NSMutableArray()
                        array_TutorialVideo = aryTemp.mutableCopy()as! NSMutableArray
                        self.array_TV = array_TutorialVideo.mutableCopy()as! NSMutableArray
                        self.tvForVideo.isHidden = false
                        self.tvForVideo.reloadData()
                    }else{
                        self.tvForVideo.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "VideoTutorial")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeature_IsFavouriteByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Videotutorial")
                            saveDataInLocal(strEntity: "Videotutorial", strKey: "video", data: aryTemp)
                            array_TutorialVideo = NSMutableArray()
                            array_TutorialVideo = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = array_TutorialVideo.mutableCopy()as! NSMutableArray
                            self.tvForVideo.delegate = self
                            self.tvForVideo.dataSource = self
                            self.tvForVideo.reloadData()
                            if self.array_TV.count != 0{
                                self.tvForVideo.isHidden = false
                            }else{
                                self.tvForVideo.isHidden = true
                            }
                        }else{
                            self.array_TV = array_TutorialVideo.mutableCopy()as! NSMutableArray
                            self.tvForVideo.delegate = self
                            self.tvForVideo.dataSource = self
                            self.tvForVideo.reloadData()
                            if self.array_TV.count != 0{
                                self.tvForVideo.isHidden = false
                            }else{
                                self.tvForVideo.isHidden = true
                            }
                        }
                    }
                }
            }
            else{
                self.array_TV = array_TutorialVideo.mutableCopy()as! NSMutableArray
                if self.array_TV.count != 0{
                    self.tvForVideo.isHidden = false
                    self.tvForVideo.reloadData()
                }else{
                    self.tvForVideo.isHidden = true
                    self.tvForVideo.reloadData()
                }
               
            }
        }
            //------------ For SMART FAQ Tutorial-------------
        else  if staticViewCome == 1 {
            if arySmartFAQData.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Smartfaq", strkey: "faq")
                    if ((aryTemp.count) != 0){
                        arySmartFAQData = NSMutableArray()
                        arySmartFAQData = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = arySmartFAQData.mutableCopy()as! NSMutableArray
                        self.tvForFAQ.isHidden = false
                        self.tvForFAQ.reloadData()
                    }else{
                        self.tvForFAQ.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "SmartFAQ")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeature_IsFavouriteByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            print(dictTemp)
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Smartfaq")
                            saveDataInLocal(strEntity: "Smartfaq", strKey: "faq", data: aryTemp)
                            
                            arySmartFAQData = NSMutableArray()
                            arySmartFAQData = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = arySmartFAQData.mutableCopy()as! NSMutableArray
                            self.tvForFAQ.isHidden = false
                            self.tvForFAQ.reloadData()
                        }else{
                            self.array_TV = arySmartFAQData.mutableCopy()as! NSMutableArray
                            self.tvForFAQ.isHidden = false
                            self.tvForFAQ.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = arySmartFAQData.mutableCopy()as! NSMutableArray
                self.tvForFAQ.isHidden = false
                self.tvForFAQ.reloadData()
            }
            
        }
            //----------------- For ACT--------------------
            
        else  if staticViewCome == 2 {
            if aryActData.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Actsdata", strkey: "act")
                   // print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        aryActData = NSMutableArray()
                        aryActData = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryActData.mutableCopy()as! NSMutableArray
                        self.tvForACTS.isHidden = false
                        self.tvForACTS.reloadData()
                    }else{
                        self.tvForACTS.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "GSTModalLaw")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeatureByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            print(dictTemp)
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Actsdata")
                            saveDataInLocal(strEntity: "Actsdata", strKey: "act", data: aryTemp)
                            
                            aryActData = NSMutableArray()
                            aryActData = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryActData.mutableCopy()as! NSMutableArray
                            self.tvForACTS.isHidden = false
                            self.tvForACTS.reloadData()
                        }else{
                            self.array_TV = aryActData.mutableCopy()as! NSMutableArray
                            self.tvForACTS.isHidden = false
                            self.tvForACTS.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = aryActData.mutableCopy()as! NSMutableArray
                self.tvForACTS.isHidden = false
                self.tvForACTS.reloadData()
            }
        }
            //----------------- For RULES--------------------
        else  if staticViewCome == 3{
            if aryRulesData.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Rules", strkey: "rulesgst")
                    //print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        aryRulesData = NSMutableArray()
                        aryRulesData = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryRulesData.mutableCopy()as! NSMutableArray
                        self.tvForRULES.isHidden = false
                        self.tvForRULES.reloadData()
                    }else{
                        self.tvForRULES.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Please wait...")
                    let soapMessage = creatSoapMessage(str: "GSTRules")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeature_IsFavouriteByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                           // print(dictTemp)
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Rules")
                            saveDataInLocal(strEntity: "Rules", strKey: "rulesgst", data: aryTemp)
                            
                            aryRulesData = NSMutableArray()
                            aryRulesData = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryRulesData.mutableCopy()as! NSMutableArray
                            self.tvForRULES.isHidden = false
                            self.tvForRULES.reloadData()
                        }else{
                            self.array_TV = aryRulesData.mutableCopy()as! NSMutableArray
                            self.tvForRULES.isHidden = false
                            self.tvForRULES.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = aryRulesData.mutableCopy()as! NSMutableArray
                self.tvForRULES.isHidden = false
                self.tvForRULES.reloadData()
            }
        }
    //----------------- For SCHEDULEDATA--------------------
            
        else if staticViewCome == 4 {
            if arySchedulesData.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Schedule", strkey: "scheduledata")
                   // print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        arySchedulesData = NSMutableArray()
                        arySchedulesData = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = arySchedulesData.mutableCopy()as! NSMutableArray
                        self.tvForSCHEDULE.isHidden = false
                        self.tvForSCHEDULE.reloadData()
                    }else{
                        self.tvForSCHEDULE.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "GSTSchedules")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeature_IsFavouriteByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            print(dictTemp)
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Schedule")
                            saveDataInLocal(strEntity: "Schedule", strKey: "scheduledata", data: aryTemp)
                            
                            arySchedulesData = NSMutableArray()
                            arySchedulesData = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = arySchedulesData.mutableCopy()as! NSMutableArray
                            self.tvForSCHEDULE.isHidden = false
                            self.tvForSCHEDULE.reloadData()
                        }else{
                            self.array_TV = aryRulesData.mutableCopy()as! NSMutableArray
                            self.tvForSCHEDULE.isHidden = false
                            self.tvForSCHEDULE.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = arySchedulesData.mutableCopy()as! NSMutableArray
                self.tvForSCHEDULE.isHidden = false
                self.tvForSCHEDULE.reloadData()
            }
            
        }
            
//----------------- For SGST NOTIFIVCATION DATA--------------------
        else if staticViewCome == 5 {
            
            if aryForSGST.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "SgstNoti", strkey: "sgstNotification")
                    // print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        aryForSGST = NSMutableArray()
                        aryForSGST = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryForSGST.mutableCopy()as! NSMutableArray
                        self.tvForSgstNotification.isHidden = false
                        self.tvForSgstNotification.reloadData()
                    }else{
                        self.tvForSgstNotification.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                   let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGSTNotification xmlns='http://gst.citizencop.org/'><Language>English</Language></GetGSTNotification></soap12:Body></soap12:Envelope>"
                    
                    WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetGSTNotificationResult", responcetype: "GetGSTNotificationResponse", OnResultBlock: { (result, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = result.value(forKey: "data")as! NSDictionary
                            let aryTemp = dictTemp.value(forKey: "GSTNotification")as! NSArray
                            deleteAllRecords(strEntity:"SgstNoti")
                            saveDataInLocal(strEntity: "SgstNoti", strKey: "sgstNotification", data: aryTemp)
                            aryForSGST = NSMutableArray()
                            aryForSGST = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryForSGST.mutableCopy()as! NSMutableArray
                            self.tvForSgstNotification.isHidden = false
                            self.tvForSgstNotification.reloadData()
                        }else{
                            self.array_TV = aryForSGST.mutableCopy()as! NSMutableArray
                            self.tvForSgstNotification.isHidden = false
                            self.tvForSgstNotification.reloadData()
                        }
                    })
                }
            }
            else{
                self.array_TV = aryForSGST.mutableCopy()as! NSMutableArray
                self.tvForSgstNotification.isHidden = false
                self.tvForSgstNotification.reloadData()
            }
            
        }
        //----------------- For SGST GST RATE DATA--------------------
        else if staticViewCome == 6 {
            
            if aryForGoodTax.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Goods", strkey: "goodsdata")
                    // print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        aryForGoodTax = NSMutableArray()
                        aryForGoodTax = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                        self.tvForRates.isHidden = false
                        self.tvForRates.reloadData()
                    }else{
                        self.tvForRates.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGstItemList xmlns='http://gst.citizencop.org/'><ItemType>Goodstax</ItemType></GetGstItemList></soap12:Body></soap12:Envelope>"
                    
                    WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetGstItemListResult", responcetype: "GetGstItemListResponse", OnResultBlock: { (result, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = result.value(forKey: "data")as! NSDictionary
                            print(dictTemp)
                            let aryTemp = dictTemp.value(forKey: "GstItemList")as! NSArray
                            deleteAllRecords(strEntity:"Goods")
                            saveDataInLocal(strEntity: "Goods", strKey: "goodsdata", data: aryTemp)
                            aryForGoodTax = NSMutableArray()
                            aryForGoodTax = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                            self.tvForRates.isHidden = false
                            self.tvForRates.reloadData()
                        }else{
                            self.array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                            self.tvForRates.isHidden = false
                            self.tvForRates.reloadData()
                        }
                    })
                }
            }
            else{
                self.array_TV = aryForGoodTax.mutableCopy()as! NSMutableArray
                self.tvForRates.isHidden = false
                self.tvForRates.reloadData()
            }
            lblGood.backgroundColor = hexStringToUIColor(hex: "2F5DD1")
            lblServices.backgroundColor = UIColor.clear
        }
            
            
            
//----------------- For Calculator DATA--------------------
        else if staticViewCome == 7 {
            self.array_DropDownTV =  NSMutableArray()
            if aryForCalculator.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Calculator", strkey: "calculatordata")
                    // print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        aryForCalculator = NSMutableArray()
                        aryForCalculator = aryTemp.mutableCopy()as! NSMutableArray
                        array_DropDownTV = aryForCalculator.mutableCopy()as! NSMutableArray
                        self.tvDropDown.isHidden = false
                        self.tvDropDown.reloadData()
                    }else{
                        self.tvDropDown.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGstItemList xmlns='http://gst.citizencop.org/'><ItemType>Goodstax</ItemType></GetGstItemList></soap12:Body></soap12:Envelope>"
                    
                    WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetGstItemListResult", responcetype: "GetGstItemListResponse", OnResultBlock: { (result, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = result.value(forKey: "data")as! NSDictionary
                            print(dictTemp)
                            let aryTemp = dictTemp.value(forKey: "GstItemList")as! NSArray
                            deleteAllRecords(strEntity:"Calculator")
                            saveDataInLocal(strEntity: "Calculator", strKey: "calculatordata", data: aryTemp)
                            aryForCalculator = NSMutableArray()
                            aryForCalculator = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_DropDownTV = aryForCalculator.mutableCopy()as! NSMutableArray
                            self.tvDropDown.reloadData()
                        }else{
                            self.array_DropDownTV = aryForCalculator.mutableCopy()as! NSMutableArray
                            self.tvDropDown.reloadData()
                        }
                    })
                }
            }
            else{
                self.array_DropDownTV = aryForCalculator.mutableCopy()as! NSMutableArray
                self.tvDropDown.reloadData()
            }
             self.tvDropDown.isHidden = true
             self.txtSearch.isEnabled = false
        }
            
//----------------- For NEWS DATA--------------------
        else if (staticViewCome == 8){
            if aryForNews.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "News", strkey: "newsdata")
                    if ((aryTemp.count) != 0){
                        aryForNews = NSMutableArray()
                        aryForNews = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryForNews.mutableCopy()as! NSMutableArray
                        self.tvForNews.isHidden = false
                        self.tvForNews.reloadData()
                    }else{
                        self.tvForNews.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "News")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeature_IsFavouriteByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"News")
                            saveDataInLocal(strEntity: "News", strKey: "newsdata", data: aryTemp)
                            aryForNews = NSMutableArray()
                            aryForNews = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryForNews.mutableCopy()as! NSMutableArray
                            self.tvForNews.isHidden = false
                            self.tvForNews.reloadData()
                        }else{
                            self.array_TV = aryForNews.mutableCopy()as! NSMutableArray
                            self.tvForNews.isHidden = false
                            self.tvForNews.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = aryForNews.mutableCopy()as! NSMutableArray
                self.tvForNews.isHidden = false
                self.tvForNews.reloadData()
            }
        
        }
//----------------- For ARTICLE DATA--------------------

        else  if staticViewCome == 9 {
            if aryArticles.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Articles", strkey: "articledata")
                    // print(aryTemp.count)
                    if ((aryTemp.count) != 0){
                        aryArticles = NSMutableArray()
                        aryArticles = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryArticles.mutableCopy()as! NSMutableArray
                        self.tvArticles.isHidden = false
                        self.tvArticles.reloadData()
                    }else{
                        self.tvArticles.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "Article")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "GetAllFeature_IsFavouriteByFeatureType") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Articles")
                            saveDataInLocal(strEntity: "Articles", strKey: "articledata", data: aryTemp)
                            
                            aryArticles = NSMutableArray()
                            aryArticles = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryArticles.mutableCopy()as! NSMutableArray
                            self.tvArticles.isHidden = false
                            self.tvArticles.reloadData()
                        }else{
                            self.array_TV = aryArticles.mutableCopy()as! NSMutableArray
                            self.tvArticles.isHidden = false
                            self.tvArticles.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = aryArticles.mutableCopy()as! NSMutableArray
                self.tvArticles.isHidden = false
                self.tvArticles.reloadData()
            }
        }
            
  //--------- For EXPERT CONTRIBUTION DATA--------------------
            
        else  if staticViewCome == 11 {
            if aryExperContribution.count == 0 {
                if !(checkInternetConnection()){
                    aryTemp = getDataFromLocal(strEntity: "Expertcontribution", strkey: "expert")
                    if ((aryTemp.count) != 0){
                        aryExperContribution = NSMutableArray()
                        aryExperContribution = aryTemp.mutableCopy()as! NSMutableArray
                        array_TV = aryExperContribution.mutableCopy()as! NSMutableArray
                        self.tvExpertCombination.isHidden = false
                        self.tvExpertCombination.reloadData()
                    }else{
                        self.tvExpertCombination.isHidden = true
                    }
                }
                else{
                    KRProgressHUD.show(message: "Loading...")
                    let soapMessage = creatSoapMessage(str: "ShareYourKnowledge")
                    WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, type: "ShareYourKnowledge") { (dictData, status) in
                        KRProgressHUD.dismiss()
                        if status == "Suceess"{
                            let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                            let aryTemp = dictTemp.value(forKey: "FeatureDataList")as! NSArray
                            deleteAllRecords(strEntity:"Expertcontribution")
                            saveDataInLocal(strEntity: "Expertcontribution", strKey: "expert", data: aryTemp)
                            aryExperContribution = NSMutableArray()
                            aryExperContribution = aryTemp.mutableCopy()as! NSMutableArray
                            self.array_TV = aryExperContribution.mutableCopy()as! NSMutableArray
                            self.tvExpertCombination.isHidden = false
                            self.tvExpertCombination.reloadData()
                        }else{
                            self.array_TV = aryExperContribution.mutableCopy()as! NSMutableArray
                            self.tvExpertCombination.isHidden = false
                            self.tvExpertCombination.reloadData()
                        }
                    }
                }
            }
            else{
                self.array_TV = aryExperContribution.mutableCopy()as! NSMutableArray
                self.tvExpertCombination.isHidden = false
                self.tvExpertCombination.reloadData()
            }
        }
        let sWidth = (Int(self.scrollContain.frame.size.width) * staticViewCome)
        let point = CGPoint(x: sWidth, y: 0)
        self.scrollContain.contentOffset = point
        self.scrollContain.isHidden = false
    }
 }
//MARK:- UITableView Delegate Methods
//MARK:-
extension  TutorialVideoVC  : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tvDropDown {
            return array_DropDownTV.count
        }else{
            return array_TV.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tvDropDown {
            let cell = tvDropDown.dequeueReusableCell(withIdentifier: "cellDrop", for: indexPath as IndexPath)as! CommonTVCell
            let dict = array_DropDownTV.object(at: indexPath.row)as! NSDictionary
            //print(dict)
            let strItemName = dict.value(forKey: "ItemName") as? String
            cell.lbl_CalculatorTitle.text = strItemName
            return cell
            
        }else{
        
            
            //------------------ForVideoTutorial-------------------//
            
            if  (staticViewCome == 0) {
                let cell = tvForVideo.dequeueReusableCell(withIdentifier: "VedioCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                let strTitle = dict.value(forKey: "Title") as? String
                var strSubTitle = dict.value(forKey: "Description") as? String
                let strDate = dict.value(forKey: "CreatedDate") as? String
                let strUrl = dict.value(forKey: "MediaPath") as? String
                
                strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
                cell.lblVedioTitle.text = strTitle
                cell.lblVedioSubTitle.text = strSubTitle
                cell.lblVediodateTime.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
                let fullNameArr = strUrl?.components(separatedBy: "=")
                var surname = fullNameArr?[1]
                surname = "http://img.youtube.com/vi/\(surname!)/1.jpg"
                cell.imgVedioProfile.setImageWith(URL(string: surname!), placeholderImage: UIImage(named: "defult_video_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
                return cell
            }
                
  //-----------------FOR SMART FAQ----------------------//
                
            else if (staticViewCome == 1){
                let cell = tvForFAQ.dequeueReusableCell(withIdentifier: "faqCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                let strTitle = dict.value(forKey: "Question") as? String
                cell.lbl_FAQ_Title.text = strTitle
                return cell
            }
                
 //-----------------FOR ACTS------------------------//
                
            else if (staticViewCome == 2){
                let cell = tvForACTS.dequeueReusableCell(withIdentifier: "actsCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                //print(dict)
                let strTitle = dict.value(forKey: "Title") as? String
                cell.lbl_ActTitle.text = strTitle
                // print(dict)
                return cell
            }
                
  //-----------------FOR RULES------------------------//
                
            else if (staticViewCome == 3){
                let cell = tvForRULES.dequeueReusableCell(withIdentifier: "rulesCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                let strTitle = dict.value(forKey: "Title") as? String
                cell.lbl_RulesTitle.text = strTitle
                return cell
                
            }
                
//-----------------FOR SCHEDULE------------------------//
                
            else if (staticViewCome == 4){
                let cell = tvForSCHEDULE.dequeueReusableCell(withIdentifier: "scheduleCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                let strTitle = dict.value(forKey: "Title") as? String
                cell.lbl_ScheduleTitle.text = strTitle
                return cell
            }
//-----------------FOR SGST NOTIFICATION------------------------//
                
            else if (staticViewCome == 5){
                let cell = tvForSgstNotification.dequeueReusableCell(withIdentifier: "SGSTCELL", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                print(dict)
                let strTitle = dict.value(forKey: "Title") as? String
                cell.lbl_SGSTNotiTitle.text = strTitle!
                return cell
            }
//-----------------FOR GOOD SERVICES---------------------//
                
            else if (staticViewCome == 6){
                let cell = tvForRates.dequeueReusableCell(withIdentifier: "RatesCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                print(dict)
                let strItemName = dict.value(forKey: "ItemName") as? String
                let strItemHSNCode = dict.value(forKey: "HSNCode") as? String
                let strGstPercent = dict.value(forKey: "GstPercent") as? String
                let strCondition = dict.value(forKey: "Condition") as? String
                
                cell.lbl_RatesItemName.text = strItemName
                cell.lbl_RatesTax.text = strGstPercent
                cell.lbl_Ratescode.text = strItemHSNCode
                if (strCondition != nil ) {
                    cell.lbl_RatesConditions.text = strCondition
                    if strCondition == "" {
                        cell.lbl_RatesConditionsStatic.text = ""
                    }else{
                        cell.lbl_RatesConditionsStatic.text = "Conditions :"
                    }
                }else{
                    cell.lbl_RatesConditions.text = ""
                    cell.lbl_RatesConditionsStatic.text = ""
                }
                if self.tvForRates.tag == 10 {
                    cell.lbl_RatescodeStatic.text = "HSN Code"
                }else{
                    cell.lbl_RatescodeStatic.text = "SAC Code"
                }
                return cell
            }
                
                //-----------------FOR News------------------------//
                
            else if (staticViewCome == 8) {
                let cell = tvForNews.dequeueReusableCell(withIdentifier: "VedioCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                print(dict)
                let strTitle = dict.value(forKey: "Title") as? String
                var strSubTitle = dict.value(forKey: "Message") as? String
                let strDate = dict.value(forKey: "CreatedDate") as? String
                var strUrl = dict.value(forKey: "ImagePath") as? String
                strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
                cell.lblVedioTitle.text = strTitle
                cell.lblVedioSubTitle.text = strSubTitle
                cell.lblVediodateTime.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
                strUrl = (strUrl! as NSString).replacingOccurrences(of: "~", with: "")
                strUrl = "http://gst.citizencop.org/\(strUrl!)"
                cell.imgVedioProfile.setImageWith(URL(string: strUrl!), placeholderImage: UIImage(named: "defult_news_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
                return cell
            }
                
                //-----------------FOR ARTICLS------------------------//
                
            else if (staticViewCome == 9) {
                let cell = tvArticles.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                let strTitle = dict.value(forKey: "Title") as? String
                var strSubTitle = dict.value(forKey: "Description") as? String
                let strDate = dict.value(forKey: "CreatedDate") as? String
                strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
                cell.lbl_ArticleTitle.text = strTitle
                cell.lbl_ArticleSUBTitle.text = strSubTitle
                cell.lbl_ArticleDate.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
                return cell
                
            }
                //-----------------FOR EXPERT CONTRIBUTION------------------------//
            else if (staticViewCome == 11) {
                let cell = tvExpertCombination.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath as IndexPath) as! CommonTVCell
                let dict = array_TV.object(at: indexPath.row)as! NSDictionary
                print(dict)
                let strTitle = dict.value(forKey: "Title") as? String
                var strSubTitle = dict.value(forKey: "Description") as? String
                let strDate = dict.value(forKey: "Created_Date") as? String
                strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
                cell.lbl_ArticleTitle.text = strTitle
                cell.lbl_ArticleSUBTitle.text = strSubTitle
                cell.lbl_ArticleDate.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
                return cell
            }
            else{
                let cell = tvForVideo.dequeueReusableCell(withIdentifier: "VedioCell", for: indexPath as IndexPath) as! CommonTVCell
                return cell
            }

        }
     }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if((tableView == tvForVideo) || (tableView == tvForNews)){
            return 142
        }else if ((tableView ==  tvArticles) ||  (tableView ==  tvExpertCombination) || (tableView ==  tvForRates)) {
            return UITableViewAutomaticDimension
        }else if ((tableView == tvDropDown)) {
            return 50
        }else {
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if tableView == tvDropDown {
            let dict = array_DropDownTV.object(at: indexPath.row)as! NSDictionary
            let strItemName = dict.value(forKey: "ItemName") as? String
            let strItemHSNCode = dict.value(forKey: "HSNCode") as? String
            let strGstPercent = dict.value(forKey: "GstPercent") as? String
            txtCalculatorDiscription.text = strItemName
            txtCalculatorCode.text = strItemHSNCode
            lblCalculatorrate.text = strGstPercent
            self.tvDropDown.isHidden = true
            self.view.endEditing(true)

        }else{
            if staticViewCome == 0 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoTutorialDetailVC") as!VideoTutorialDetailVC
                let dict = array_TutorialVideo.object(at: indexPath.row)as! NSDictionary
                vc.strcome = "Video"
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 1){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
                vc.strCome = "FAQ"
                let dict = arySmartFAQData.object(at: indexPath.row)as! NSDictionary
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 2){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskVC") as! HelpDeskVC
                vc.strViewComeFrom = "ACT"
                let dict = aryActData.object(at: indexPath.row)as! NSDictionary
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 3){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
                vc.strCome = "RULES"
                let dict = aryRulesData.object(at: indexPath.row)as! NSDictionary
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 4){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
                vc.strCome = "Schedules"
                let dict = arySchedulesData.object(at: indexPath.row)as! NSDictionary
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 5){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "About_Disclaimber_DeveloperVC") as! About_Disclaimber_DeveloperVC
                vc.strViewComeFrom = "Home"
                strTagADD = "SGST Notification"
                vc.dictData = array_TV.object(at: indexPath.row)as! NSDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 6){
                viewRatesDetail.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.viewRatesDetail.transform = CGAffineTransform(scaleX: 1,y: 1)
                    self.viewtrans.isHidden = false
                    self.viewRatesDetail.isHidden = false
                    let dictdata = self.array_TV.object(at: indexPath.row)as! NSDictionary
                    self.lblTax.text = "Tax : \(dictdata.value(forKey: "GstPercent") as! String)"
                    self.lblHsnCode.text = "HSN Code : \(dictdata.value(forKey: "HSNCode") as! String)"
                    self.txtItemname.text = "Item Name :\(dictdata.value(forKey: "ItemName") as! String)"
                    let strconditions = dictdata.value(forKey: "Condition")as? String
                    if (strconditions != nil)  {
                        self.txtComments.text = "Condition :\(strconditions!)"
                    }else{
                        self.txtComments.text = "Condition : -"
                    }
                })
                
            }else if(staticViewCome == 8){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoTutorialDetailVC") as!VideoTutorialDetailVC
                let dict = aryForNews.object(at: indexPath.row)as! NSDictionary
                vc.strcome = "News"
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 9){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
                vc.strCome = "Article"
                let dict = aryArticles.object(at: indexPath.row)as! NSDictionary
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else if(staticViewCome == 11){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
                vc.strCome = "ExpertContribution"
                let dict = aryExperContribution.object(at: indexPath.row)as! NSDictionary
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }
            

        }
      
    }
    
}
//MARK:- UITextViewDelegate  Methods
//MARK:-
extension TutorialVideoVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtDescription {
            if (txtDescription?.text == "Enter question here") {
                txtDescription!.text = nil
                txtDescription!.textColor = UIColor.black
            }
        }else if (textView == txtCalculatorDiscription){
            txtCalculatorDiscription.text = "Description"
            txtCalculatorDiscription!.text = nil
            txtCalculatorDiscription!.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtDescription {
            let strDesc = txtDescription?.text.trimmingCharacters(in: .whitespacesAndNewlines)
            if strDesc == "" {
                txtDescription?.text = "Enter question here"
                txtDescription?.textColor = UIColor.lightGray
            } else {
                txtDescription?.textColor = UIColor.black
            }
        }else if (textView == txtCalculatorDiscription){
            let strDesc = txtCalculatorDiscription?.text.trimmingCharacters(in: .whitespacesAndNewlines)
            if strDesc == "" {
                txtCalculatorDiscription?.text = "Description"
                txtCalculatorDiscription?.textColor = UIColor.lightGray
            } else {
                txtCalculatorDiscription?.textColor = UIColor.black
                tvDropDown.isHidden = true
            }

        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentCharacterCount = textView.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        if textView == txtDescription {
            if (text == "\n") {
                self.view.endEditing(true)
                return false
            }
            let strDesc = txtDescription.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if strDesc != "" {
            } else {
                txtDescription?.text = ""
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 200
            
        }else if (textView == txtCalculatorDiscription){
       
                if (text == "\n") {
                    self.view.endEditing(true)
                    return false
                }
                let strDesc = txtCalculatorDiscription.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                if strDesc != "" {
                    self.searchAutocomplete_DropDown(Searching: strDesc! as NSString)
                } else {
                    txtCalculatorDiscription?.text = ""
                    self.tvDropDown.isHidden = true
                }
                return true
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.txtSearch.text = ""
        return true
    }
}
//MARK: - UITextFieldDelegate
//MARK: -

extension TutorialVideoVC: UITextFieldDelegate  {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        if staticViewCome == 0 {
            self.searchAutocomplete_Video(Searching: txtAfterUpdate)
        }else if (staticViewCome == 1) {
            self.searchAutocomplete_SMARTFAQ(Searching: txtAfterUpdate)
        }else if (staticViewCome == 3){
            self.searchAutocomplete_RULES(Searching: txtAfterUpdate)
        }else if (staticViewCome == 2){
            self.searchAutocomplete_ACTS(Searching: txtAfterUpdate)
        }
        else if (staticViewCome == 4){
            self.searchAutocomplete_SCHEDULE(Searching: txtAfterUpdate)
        } else if (staticViewCome == 5){
            self.searchAutocomplete_SGST(Searching: txtAfterUpdate)
        } else if (staticViewCome == 8){
            self.searchAutocomplete_News(Searching: txtAfterUpdate)
        }else if (staticViewCome == 9){
            self.searchAutocomplete_Articla(Searching: txtAfterUpdate)
        }else if (staticViewCome == 11){
            self.searchAutocomplete_ExpertContribution(Searching: txtAfterUpdate)
        }else if (staticViewCome == 6){
            if tvForRates.tag == 10 {
                self.searchAutocomplete_RATESGOODS(Searching: txtAfterUpdate)
            }else{
                self.searchAutocomplete_SERVICESGOODS(Searching: txtAfterUpdate)
            }
        }
        print("\(txtAfterUpdate)")
        return true
}
    
    func searchAutocomplete_Video(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Description contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (array_TutorialVideo ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForVideo.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = array_TutorialVideo.mutableCopy() as! NSMutableArray
            self.tvForVideo.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_SMARTFAQ(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Question contains[c] %@ OR Question contains[c] %@", argumentArray: [Searching, Searching])

        if !(Searching.length == 0) {
            let arrayfilter = (arySmartFAQData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForFAQ.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = arySmartFAQData.mutableCopy() as! NSMutableArray
            self.tvForFAQ.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    
    
    func searchAutocomplete_RULES(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryRulesData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForRULES.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryRulesData.mutableCopy() as! NSMutableArray
            self.tvForRULES.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    
    
    func searchAutocomplete_ACTS(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryActData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForACTS.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryActData.mutableCopy() as! NSMutableArray
            self.tvForACTS.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }

    
    
    func searchAutocomplete_SCHEDULE(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (arySchedulesData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForSCHEDULE.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = arySchedulesData.mutableCopy() as! NSMutableArray
            self.tvForSCHEDULE.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_Articla(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Description contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryArticles ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvArticles.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryArticles.mutableCopy() as! NSMutableArray
            self.tvArticles.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_News(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Message contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryForNews ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForNews.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryForNews.mutableCopy() as! NSMutableArray
            self.tvForNews.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_ExpertContribution(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Description contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryExperContribution ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvExpertCombination.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryExperContribution.mutableCopy() as! NSMutableArray
            self.tvExpertCombination.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_SGST(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryForSGST ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForSgstNotification.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryForSGST.mutableCopy() as! NSMutableArray
            self.tvForSgstNotification.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    
    func searchAutocomplete_RATESGOODS(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "ItemName contains[c] %@ OR HSNCode contains[c] %@ OR GstPercent contains[c] %@", argumentArray: [Searching, Searching,Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (aryForGoodTax ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForRates.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryForGoodTax.mutableCopy() as! NSMutableArray
            self.tvForRates.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    
    
    func searchAutocomplete_SERVICESGOODS(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "ItemName contains[c] %@ OR HSNCode contains[c] %@ OR GstPercent contains[c] %@", argumentArray: [Searching, Searching,Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (aryForServiceTax ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForRates.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryForServiceTax.mutableCopy() as! NSMutableArray
            self.tvForRates.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_DropDown(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "ItemName contains[c] %@ OR ItemName contains[c] %@",  argumentArray: [Searching, Searching,Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (aryForCalculator ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_DropDownTV = NSMutableArray()
            self.array_DropDownTV = nsMutableArray.mutableCopy() as! NSMutableArray
            if self.array_DropDownTV.count != 0 {
                self.tvDropDown.isHidden = false
                self.tvDropDown.reloadData()
            }else{
                self.tvDropDown.isHidden = true
            }
        }
        else{
            self.array_DropDownTV = NSMutableArray()
            self.array_DropDownTV = aryForCalculator.mutableCopy() as! NSMutableArray
            self.tvDropDown.reloadData()
            self.tvDropDown.isHidden = true
            self.view.endEditing(true)
        }
    }

}

