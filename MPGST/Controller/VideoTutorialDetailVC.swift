//
//  VideoTutorialDetailVC.swift
//  MPGST
//
//  Created by Navin Patidar on 8/16/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class VideoTutorialDetailVC: UIViewController {
    
    @IBOutlet weak var tvForVideo: UITableView!
    @IBOutlet var videoView: UIWebView!
    @IBOutlet var videoDefaultView: UIView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var lblErrormsg: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnLike_Unlike: UIButton!
    
    //News detail
    @IBOutlet var imgNewDetail: UIImageView!
    var dictData = NSMutableDictionary()
    var strcome = String()
    var strISLike = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForVideo.estimatedRowHeight = 118.0
        print(dictData)
        
        //-------For Viedo Detail--------//
        if strcome == "Video" {
            let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            lblTitle.text = "Tutorial Videos"
            setLikeUnlikeButtonImage(status: IsFavourite!, strcome: "Video")
            let strvideo = self.dictData.value(forKey: "MediaPath") as? String
            
            let fullNameArr = strvideo?.components(separatedBy: "=")
            let surname = fullNameArr?[1]
            getVideoID(str: surname!)
            videoView.delegate = self
            lblErrormsg.isHidden = true
            videoView.isHidden = true
            videoDefaultView.isHidden = false
            imgNewDetail.isHidden = true
        }
            
            //------For News Detail --------//
            
        else if(strcome == "News"){
            let IsFavourite = self.dictData.value(forKey: "IsFavourite") as? Bool
            setLikeUnlikeButtonImage(status: IsFavourite!, strcome: "News")
            lblTitle.text = "News Updates"
            imgNewDetail.isHidden = false
            lblErrormsg.isHidden = true
            videoView.isHidden = true
            videoDefaultView.isHidden = true
            var strUrl = self.dictData.value(forKey: "ImagePath") as? String
            strUrl = (strUrl! as NSString).replacingOccurrences(of: "~", with: "")
            strUrl = "http://gst.citizencop.org/\(strUrl!)"
            imgNewDetail.setImageWith(URL(string: strUrl!), placeholderImage: UIImage(named: "defult_news_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
        }
    }
    
    override func viewWillLayoutSubviews() {
        if  DeviceType.IS_IPHONE_5{
            heightConstraint.constant = 170
        }else if DeviceType.IS_IPHONE_6{
            heightConstraint.constant = 200
        }else if DeviceType.IS_IPHONE_6P{
            heightConstraint.constant = 220
        }else if DeviceType.IS_IPAD {
            heightConstraint.constant = 300
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- @IBAction
    //MARK:-
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionShare(_ sender: UIButton) {
        
        var strMessage = String()
        if strcome == "Video" {
            let strvideo = self.dictData.value(forKey: "MediaPath") as? String
            let fullNameArr = strvideo?.components(separatedBy: "=")
            let surname = fullNameArr?[1]
            strMessage = "Hi \n Must Watch! Do not miss out on To The Point with GST Commissioner Upendra Gupta. Check out the video https://www.youtube.com/watch?v=\(String(describing: surname!))"
            if let link = NSURL(string: "https://www.youtube.com/watch?v=\(String(describing: surname!))")
            {
                let objectsToShare = [strMessage,link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
            }
        }else if(strcome == "News"){
            
            strMessage = (self.dictData.value(forKey: "Message") as? String)!
            let objectsToShare = [strMessage] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    @IBAction func actionLike(_ sender: UIButton) {
        if checkInternetConnection() == true {
            
            //-------For Video Detail ---------//
            if (strcome == "Video"){
                let strItemID = self.dictData.value(forKey: "Id") as? Int
                callLikeUnlikeAPI(strItemID: "\(strItemID!)", strFavouriteType: "VideoTutorial")
            }
            //-------For News Detail ---------//

            else if (strcome == "News"){
                let strItemID = self.dictData.value(forKey: "Id") as? Int
                callLikeUnlikeAPI(strItemID: "\(strItemID!)", strFavouriteType: "News")
            }
            
        }else{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please check internet connection", viewcontrol: self)
        }
    }
    
    //MARK:- OTHER FUNCTION
    //MARK:-
    
    
    func checkItemIsFavouriteOrNot(strItemID : String , strItemType : String, strEmail : String) {
        DispatchQueue.main.async {
            print(strItemID)
            print(strItemType)
            print(strEmail)
            KRProgressHUD.show(message: "Loading...")
            let soapmsg = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFavouriteFeature xmlns='http://gst.citizencop.org/'><FavouriteTypeId>\(strItemID)</FavouriteTypeId><FavouriteType>\(strItemType)</FavouriteType><EmailId>\(nsud.value(forKey: "usermail")as!String)</EmailId></GetFavouriteFeature></soap12:Body></soap12:Envelope>"
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapmsg, url: BaseURL, resultype: "GetFavouriteFeatureResult", responcetype: "GetFavouriteFeatureResponse", OnResultBlock: { (dictData, status) in
                KRProgressHUD.dismiss()
                print(dictData)
                if status == "Suceess"{
                    let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                    if dictTemp.count > 0{
                        if dictTemp.value(forKey: "Result")as! String  == "True"{
                            self.btnLike_Unlike.isUserInteractionEnabled = false
                            self.btnLike_Unlike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
                        }else{
                            self.btnLike_Unlike.isUserInteractionEnabled = true
                            self.btnLike_Unlike.setImage(UIImage(named:"favorite_icon"), for: .normal)
                        }
                    }
                }else{
                    
                }
            })
        }
    }
    
    func callLikeUnlikeAPI(strItemID : String ,strFavouriteType : String )  {
        print(strItemID);
        print(strFavouriteType);
        
        KRProgressHUD.show(message: "Loading...")
        var soapMessage = String()
        if strcome == "Video" {
            soapMessage  = creatSoapMessage(itemid: strItemID, type: "VideoTutorial")
        }else if (strcome == "News"){
            soapMessage  = creatSoapMessage(itemid: strItemID, type: "News")
        }
        
        WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddFavouriteFeatureResult", responcetype: "AddFavouriteFeatureResponse", OnResultBlock: { (dictData, status) in
            KRProgressHUD.dismiss()
            print(dictData)
            if status == "Suceess"{
                let dictTemp = dictData.value(forKey: "data")as! NSDictionary
                if dictTemp.count > 0{
                    if dictTemp.value(forKey: "Result")as! String  == "True"{
                        self.btnLike_Unlike.isUserInteractionEnabled = false
                        self.btnLike_Unlike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
                    }
                }
                
                
            }else{
                
            }
        })
    }
    
    func creatSoapMessage(itemid : String , type : String ) -> String {
        let strEmail = nsud.value(forKey: "usermail")as!String
        print(itemid)
        print(strEmail)
        print(type)

        
        return  "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddFavouriteFeature xmlns='http://gst.citizencop.org/'><FavouriteTypeId>\(itemid)</FavouriteTypeId><IsFavourite>true</IsFavourite><FavouriteType>\(type)</FavouriteType><EmailId>\(strEmail)</EmailId></AddFavouriteFeature></soap12:Body></soap12:Envelope>"
    }
    
    
    
    func setLikeUnlikeButtonImage(status : Bool, strcome: String) {
        if status == true {
            btnLike_Unlike.isUserInteractionEnabled = false
            btnLike_Unlike.setImage(UIImage(named:"favorite_icon2"), for: .normal)
        }else{
            btnLike_Unlike.isUserInteractionEnabled = true
            btnLike_Unlike.setImage(UIImage(named:"favorite_icon"), for: .normal)
            
            if checkInternetConnection() == true {
                
                //ForCheckViedoAPI
                if strcome == "Video" {
                    let strItemID = self.dictData.value(forKey: "Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "VideoTutorial", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                    //For CheckNews
                else if(strcome == "News"){
                    let strItemID = self.dictData.value(forKey: "Id") as? Int
                    self.checkItemIsFavouriteOrNot(strItemID: "\(strItemID!)", strItemType: "News", strEmail: "\(nsud.value(forKey: "usermail")as! String)")
                }
                
                
                
            }else{
                
            }
        }
    }
    
    func getVideoID(str: String){
        
        let url = URL (string: "https://www.youtube.com/embed/\(str)")
        let requestObj = URLRequest(url: url!)
        videoView.allowsInlineMediaPlayback = true
        videoView.loadRequest(requestObj)
        // activity.isHidden = false
        
    }
}
//MARK:- UIWebViewDelegate Methods
//MARK:-
extension  VideoTutorialDetailVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        videoView.isHidden = false
        videoDefaultView.isHidden = true
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activity.isHidden = true
        videoView.isHidden = false
        videoDefaultView.isHidden = true
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activity.isHidden = true
        print(error.localizedDescription)
        lblErrormsg.isHidden = false
        lblErrormsg.text = error.localizedDescription
        videoDefaultView.isHidden = false
        videoView.isHidden = true
    }
    
}

//MARK:- UITableView Delegate Methods
//MARK:-
extension  VideoTutorialDetailVC  : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //------------------ForVideoTutorial-------------------//
        let cell = tvForVideo.dequeueReusableCell(withIdentifier: "VedioDetailCell", for: indexPath as IndexPath) as! CommonTVCell
        if strcome == "Video" {
            let strTitle = self.dictData.value(forKey: "Title") as? String
            let strSubTitle = self.dictData.value(forKey: "Description") as? String
            let strDate = self.dictData.value(forKey: "CreatedDate") as? String
            cell.lblVedioDetailTitle.text = strTitle!
            cell.txt_VideoDetail.attributedText = stringFromHtml(string: strSubTitle!)
            cell.lblVedioDetaildateTime.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
        }else if(strcome == "News"){
            let strTitle = self.dictData.value(forKey: "Title") as? String
            let strSubTitle = self.dictData.value(forKey: "Message") as? String
            let strDate = self.dictData.value(forKey: "CreatedDate") as? String
            cell.lblVedioDetailTitle.text = strTitle!
            cell.txt_VideoDetail.attributedText = stringFromHtml(string: strSubTitle!)
            cell.lblVedioDetaildateTime.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let strTitle = self.dictData.value(forKey: "Title") as? String
        //        let strSubTitle = self.dictData.value(forKey: "Description") as? String
        //        let size = calculateSizeFromString(message: strTitle!, view: self.view)
        //        let size1 = calculateSizeFromString1(message: strSubTitle!, view: self.view)
        //        let size2 = size + size1
        return tvForVideo.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
