//
//  CommonTVCell.swift
//  MPGST
//
//  Created by Navin on 8/13/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class CommonTVCell: UITableViewCell {
    
    // HELP DESK
    @IBOutlet weak var lblHelp_Title: UILabel!
    
    //HELP DETAIL
    @IBOutlet weak var lblDetail_Designation: UILabel!
    @IBOutlet weak var lblDetail_StaticDmobile: UILabel!
    @IBOutlet weak var btnDetailDMobile: UIButton!
    @IBOutlet weak var lblDeail_circle: UILabel!
    @IBOutlet weak var btnDetail_Mobile: UIButton!
    @IBOutlet weak var lblDetail_name: UILabel!
    @IBOutlet weak var lblDetail_DMobileDConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbl_detailDMobileHeightConstraint: NSLayoutConstraint!
    
    //Video&NewUpdate
    @IBOutlet weak var imgVedioProfile: UIImageView!
    @IBOutlet weak var lblVedioTitle: UILabel!
    @IBOutlet weak var lblVedioSubTitle: UILabel!
    @IBOutlet weak var lblVediodateTime: UILabel!
    
    //VideoDetail
    @IBOutlet weak var lblVedioDetailTitle: UILabel!
    @IBOutlet weak var lblVedioDetailSubTitle: UILabel!
    @IBOutlet weak var lblVedioDetaildateTime: UILabel!
    @IBOutlet weak var txt_VideoDetail: UITextView!
    
    //FAQ List
    @IBOutlet weak var lbl_FAQ_Title: UILabel!
    
    //FAQDetail & Act Detail
    @IBOutlet weak var lbl_FAQDetail_Date: UILabel!
    @IBOutlet weak var lbl_FAQDetail_Title: UILabel!
    @IBOutlet weak var lbl_FAQDetail_SubTitle: UILabel!
    @IBOutlet weak var txt_FAQDetail: UITextView!
    @IBOutlet weak var img_FaqImage: UIImageView!
    
    //ACT List
    @IBOutlet weak var lbl_ActTitle: UILabel!
    
    //RULES List
    @IBOutlet weak var lbl_RulesTitle: UILabel!
    
    //SCHEDULE List
    @IBOutlet weak var lbl_ScheduleTitle: UILabel!
    
    //Rules &Schedule Detail
    @IBOutlet weak var lbl_RulesSchedule_Date: UILabel!
    @IBOutlet weak var lbl_RulesSchedule_Title: UILabel!
    @IBOutlet weak var lbl_RulesSchedule_SubTitle: UILabel!
    @IBOutlet weak var txt_RulesSchedule: UITextView!
  
    //ACT & HelpDesk List
    @IBOutlet weak var lbl_Act_HelpTitle: UILabel!
    
    //ARTICLES List
    @IBOutlet weak var lbl_ArticleTitle: UILabel!
    @IBOutlet weak var lbl_ArticleSUBTitle: UILabel!
    @IBOutlet weak var lbl_ArticleDate: UILabel!

    //SGST Notification
    @IBOutlet weak var lbl_SGSTNotiTitle: UILabel!
    @IBOutlet weak var btnSGSTDownload: UIButton!

    //Rates
    @IBOutlet weak var lbl_RatesItemName: UILabel!
    @IBOutlet weak var lbl_Ratescode: UILabel!
    @IBOutlet weak var lbl_RatesTax: UILabel!
    @IBOutlet weak var lbl_RatesConditions: UILabel!
    @IBOutlet weak var lbl_RatesConditionsStatic: UILabel!
    @IBOutlet weak var lbl_RatescodeStatic: UILabel!
     //Calculator
    @IBOutlet weak var lbl_CalculatorTitle: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
    }
    
}
