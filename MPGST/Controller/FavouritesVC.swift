//
//  FavouritesVC.swift
//  MPGST
//
//  Created by Navin Patidar on 9/4/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class FavouritesVC: UIViewController , UIScrollViewDelegate{
    
    //MARK:
    //MARK: @VARIABLE
    var array_btnMenu = NSMutableArray()
    var array_TV = NSMutableArray()
    var strComeFrom = String()
    var strViewComeFrom : String!
    var scrollTag : Int!
    var strButtonPossition : Int!
    
    //MARK:
    //MARK: @IBOutlet
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var scrollContain: UIScrollView!
    @IBOutlet weak var txtSearch: UITextField!
    
    //View1ForVedio
    @IBOutlet weak var tvForVideo: UITableView!
    
    //View1ForFAQ
    @IBOutlet weak var tvForFAQ: UITableView!
    
    //View For ACTS
    @IBOutlet weak var tvForACTS: UITableView!
    
    //View For Article
    @IBOutlet weak var tvForArticle: UITableView!
    
    //View For newUpdate
    @IBOutlet weak var tvForNewUpdate: UITableView!
    
    //View For ExpertContribution
    @IBOutlet weak var tvForExpertContribution: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollTag = 0
        scroll.delegate = self
        scrollContain.delegate = self
        array_btnMenu = ["TUTORIAL VIDEO", "SMART FAQ", "ACTS" ,"ARTICLES" ,"NEWS UPDATES" ,"EXPERT CONTRIBUTION"]
        
        tvForVideo.isHidden = true
        tvForFAQ.isHidden = true
        tvForACTS.isHidden = true
        tvForArticle.isHidden = true
        tvForNewUpdate.isHidden = true
        tvForExpertContribution.isHidden = true
        tvForArticle.estimatedRowHeight = 150.0
        tvForExpertContribution.estimatedRowHeight = 150.0
        
        txtSearch.delegate = self
        if checkInternetConnection() == true {
            callAPIForGetAllFeature()
            setUPMenuTap()
            
        }else{
            let alert = UIAlertController(title: "Alert!", message: "Please check internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                if self.strViewComeFrom != "Home" {
                    let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        scrollContain.contentSize = CGSize(width: scrollContain.frame.size.width*6, height: scrollContain.frame.size.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    //MARK:
    //MARK: @IBAction
    
    @IBAction func actionBack(_ sender: UIButton) {
        if strViewComeFrom != "Home" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:
    //MARK: OTHER FUNCTION
    func setUPMenuTap(){
        array_TV = NSMutableArray()
        // self.scrollContain.isHidden = true
        let subViews = self.scroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        self.view.endEditing(true)
        var iWidthButton: CGFloat = 0.0
        var totalx: CGFloat = 0.0
        for i in 0..<array_btnMenu.count {
            let btn_Menu = UIButton()
            let lblIndicate = UILabel()
            let strTitle = array_btnMenu[i]as! String
            btn_Menu.setTitle(strTitle as String?, for: .normal)
            btn_Menu.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
            btn_Menu.setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
            let maxLabelSize: CGSize = CGSize(width: 300,height: CGFloat(9999))
            let contentNSString = strTitle as NSString
            let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)], context: nil)
            iWidthButton = expectedLabelSize.size.width + 40
            btn_Menu.frame = CGRect(x: totalx, y: 0, width: iWidthButton - 1, height:scroll.frame.size.height)
            lblIndicate.frame = CGRect(x: totalx, y: scroll.frame.size.height - 2  , width: iWidthButton - 1, height:scroll.frame.size.height)
            totalx = totalx + iWidthButton
            self.scroll.layer.cornerRadius = 0.0
            btn_Menu.tag = i
            lblIndicate.tag = i
            btn_Menu.addTarget(self, action: #selector(self.action_ChangeMenu), for: .touchUpInside)
            scroll.addSubview(btn_Menu)
            scroll.addSubview(lblIndicate)
            if i == scrollTag {
                strButtonPossition =  Int (btn_Menu.frame.origin.x)
                btn_Menu.setTitleColor(UIColor.black, for: UIControlState.normal)
                lblIndicate.backgroundColor = hexStringToUIColor(hex: "26336B")
            }
            else {
                btn_Menu.setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
                lblIndicate.backgroundColor = UIColor.white
            }
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.scroll.contentSize  = CGSize(width: totalx, height: self.scroll.frame.size.height)
            if self.strButtonPossition == 0{
                self.scroll.contentOffset.x = CGFloat(self.strButtonPossition)
            }else{
                self.scroll.contentOffset.x = CGFloat(self.strButtonPossition - 100)
            }
        }, completion: nil)
        
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.setUpTableData(strTag: scrollTag)
    }
    
    func action_ChangeMenu(sender: AnyObject) {
        let subviews : NSArray = self.scroll.subviews as NSArray
        for btn_Menu in subviews{
            if (btn_Menu is UIButton) {
                if (btn_Menu as! UIButton).tag == sender.tag {
                    (btn_Menu as AnyObject).setTitleColor(UIColor.black, for: UIControlState.normal)
                    
                    let sWidth = (Int(self.scrollContain.frame.size.width) * sender.tag )
                    scrollTag = sender.tag
                    self.scrollContain.contentOffset.x = CGFloat(sWidth)
                    setUPMenuTap()
                    
                }
                else {
                    (btn_Menu as AnyObject).setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
                }
            }else if(btn_Menu is UILabel){
                if (btn_Menu as! UILabel).tag == sender.tag {
                    (btn_Menu as! UILabel).backgroundColor = hexStringToUIColor(hex: "26336B")
                }
                else {
                    (btn_Menu as! UILabel).backgroundColor  = UIColor.white
                }
            }
        }
    }
    
    func creatSoapMessage(str : String) -> String {
        
        let strUserEmail = nsud.value(forKey: "usermail")as! String
        return  "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllFavouriteFeature xmlns='http://gst.citizencop.org/'><EmailId>\(strUserEmail)</EmailId></GetAllFavouriteFeature></soap12:Body></soap12:Envelope>"
    }
    
    
    func callAPIForGetAllFeature() {
        let strSoapMessage = self.creatSoapMessage(str: "")
        KRProgressHUD.show(message: "Loading...")
        
        WebService.callSOAP_APIBY_GETHELP(soapMessage: strSoapMessage, url: BaseURL, resultype: "GetAllFavouriteFeatureResult", responcetype: "GetAllFavouriteFeatureResponse") { (dictData, status) in
            KRProgressHUD.dismiss()
            print(dictData)
            if status == "Suceess"{
                if dictData.count>0{
                    let dict = dictData.value(forKey: "data")as! NSDictionary
                    
                    if ((dict.value(forKey: "GalleryFavourite")as? String) != nil){

                        
                    }
                    else {
                        aryFav_TutorialVideo = NSMutableArray()
                        aryFav_TutorialVideo = (dict.value(forKey: "GalleryFavourite")as! NSArray).mutableCopy()as! NSMutableArray
                        aryFav_SmartFaq = NSMutableArray()
                        aryFav_SmartFaq = (dict.value(forKey: "SmartFAQFavourite")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        aryFav_NewsUpdate = NSMutableArray()
                        aryFav_NewsUpdate = (dict.value(forKey: "NewsFavourite")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        aryFav_Acts = NSMutableArray()
                        aryFav_Acts = (dict.value(forKey: "GSTModalLawFavourite")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        aryFav_Articles = NSMutableArray()
                        aryFav_Articles = (dict.value(forKey: "ArticleFavourite")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        aryFav_ExpertContribution = NSMutableArray()
                        aryFav_ExpertContribution = (dict.value(forKey: "YourKnowledgeFavourite")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        self.setUpTableData(strTag: 0)
                    }
                }
            }else{
                
                
            }
        }
        
    }
    
    //MARK:
    //MARK: WEBSERVICES CALLING
    func setUpTableData(strTag : Int){
        print("Calling : \(strTag)")
        self.view.endEditing(true)
        //For Video
        
        if strTag == 0 {
            array_TV = NSMutableArray()
            array_TV = aryFav_TutorialVideo.mutableCopy()as! NSMutableArray
            self.tvForVideo.reloadData()
            tvForVideo.isHidden = false
        }
        //For FAQ
            
        else if(strTag == 1 ){
            array_TV = NSMutableArray()
            array_TV = aryFav_SmartFaq.mutableCopy()as! NSMutableArray
            self.tvForFAQ.reloadData()
            tvForFAQ.isHidden = false
        }
        //For ACTS
            
        else if(strTag == 2 ){
            array_TV = NSMutableArray()
            array_TV = aryFav_Acts.mutableCopy()as! NSMutableArray
            self.tvForACTS.reloadData()
            tvForACTS.isHidden = false
            
        }
        //For Articles
            
        else if(strTag == 3 ){
            array_TV = NSMutableArray()
            array_TV = aryFav_Articles.mutableCopy()as! NSMutableArray
            self.tvForArticle.reloadData()
            tvForArticle.isHidden = false
            
        }
        //For NewsUpdate
            
        else if(strTag == 4 ){
            array_TV = NSMutableArray()
            array_TV = aryFav_NewsUpdate.mutableCopy()as! NSMutableArray
            self.tvForNewUpdate.reloadData()
            tvForNewUpdate.isHidden = false
            
        }
        //For ExpertContribution
            
        else if(strTag == 5 ){
            array_TV = NSMutableArray()
            array_TV = aryFav_ExpertContribution.mutableCopy()as! NSMutableArray
            self.tvForExpertContribution.reloadData()
            tvForExpertContribution.isHidden = false
            
        }
        
    }
    //MARK:
    // MARK:-- scrollView Delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isEqual(scrollContain) {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                self.scrollContain.contentOffset.y = CGFloat(0)
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isEqual(scrollContain){
            let page =  Int(scrollView.contentOffset.x / scrollView.frame.size.width)
            scrollTag = page
            setUPMenuTap()
        }
    }
}
//MARK:- UITableView Delegate Methods
//MARK:-
extension  FavouritesVC  : UITableViewDelegate , UITableViewDataSource{
  
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_TV.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //------------------ForVideoTutorial-------------------//
        if  (scrollTag == 0) {
            
            let cell = tvForVideo.dequeueReusableCell(withIdentifier: "VedioCell", for: indexPath as IndexPath) as! CommonTVCell
            let dict = array_TV.object(at: indexPath.row)as! NSDictionary
            let strTitle = dict.value(forKey: "Title") as? String
            var strSubTitle = dict.value(forKey: "Description") as? String
            let strDate = dict.value(forKey: "CreatedDate") as? String
            let strUrl = dict.value(forKey: "MediaPath") as? String
            
            strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
            strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
            cell.lblVedioTitle.text = strTitle
            cell.lblVedioSubTitle.text = strSubTitle
            cell.lblVediodateTime.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            let fullNameArr = strUrl?.components(separatedBy: "=")
            var surname = fullNameArr?[1]
            surname = "http://img.youtube.com/vi/\(surname!)/1.jpg"
            cell.imgVedioProfile.setImageWith(URL(string: surname!), placeholderImage: UIImage(named: "defult_video_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            return cell
        }
            //-----------------FOR SMART FAQ----------------------//
            
        else if (scrollTag == 1){
            let cell = tvForFAQ.dequeueReusableCell(withIdentifier: "faqCell", for: indexPath as IndexPath) as! CommonTVCell
            let dict = array_TV.object(at: indexPath.row)as! NSDictionary
            let strTitle = dict.value(forKey: "Question") as? String
            cell.lbl_FAQ_Title.text = strTitle
            return cell
        }
            //-----------------FOR ACTS------------------------//
            
        else if (scrollTag == 2){
            let cell = tvForACTS.dequeueReusableCell(withIdentifier: "actsCell", for: indexPath as IndexPath) as! CommonTVCell
            let dict = array_TV.object(at: indexPath.row)as! NSDictionary
            let strTitle = dict.value(forKey: "Title") as? String
            cell.lbl_ActTitle.text = strTitle
            return cell
        }
            
            //-----------------FOR News------------------------//
            
        else if (scrollTag == 4) {
            let cell = tvForNewUpdate.dequeueReusableCell(withIdentifier: "VedioCell", for: indexPath as IndexPath) as! CommonTVCell
            cell.imgVedioProfile.image = UIImage(named:"defult_news_img")
            let dict = array_TV.object(at: indexPath.row)as! NSDictionary
            print(dict)
            let strTitle = dict.value(forKey: "Title") as? String
            var strSubTitle = dict.value(forKey: "Message") as? String
            let strDate = dict.value(forKey: "CreatedDate") as? String
            var strUrl = dict.value(forKey: "ImagePath") as? String
            strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
            strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
            cell.lblVedioTitle.text = strTitle
            cell.lblVedioSubTitle.text = strSubTitle
            cell.lblVediodateTime.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            strUrl = (strUrl! as NSString).replacingOccurrences(of: "~", with: "")
            strUrl = "http://gst.citizencop.org/\(strUrl!)"
            cell.imgVedioProfile.setImageWith(URL(string: strUrl!), placeholderImage: UIImage(named: "defult_news_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            return cell
        }
            
            //-----------------FOR ARTICLS------------------------//
            
        else if (scrollTag == 3) {
            let cell = tvForArticle.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath as IndexPath) as! CommonTVCell
            let dict = array_TV.object(at: indexPath.row)as! NSDictionary
            let strTitle = dict.value(forKey: "Title") as? String
            var strSubTitle = dict.value(forKey: "Description") as? String
            let strDate = dict.value(forKey: "CreatedDate") as? String
            strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
            strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
            cell.lbl_ArticleTitle.text = strTitle
            cell.lbl_ArticleSUBTitle.text = strSubTitle
            cell.lbl_ArticleDate.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            return cell
            
        }
            //-----------------FOR EXPERT CONTRIBUTION------------------------//
        else  {
            let cell = tvForExpertContribution.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath as IndexPath) as! CommonTVCell
            let dict = array_TV.object(at: indexPath.row)as! NSDictionary
            print(dict)
            let strTitle = dict.value(forKey: "Title") as? String
            var strSubTitle = dict.value(forKey: "Description") as? String
            let strDate = dict.value(forKey: "Created_Date") as? String
            strSubTitle = strSubTitle?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
            strSubTitle = strSubTitle?.trimmingCharacters(in: .newlines)
            cell.lbl_ArticleTitle.text = strTitle
            cell.lbl_ArticleSUBTitle.text = strSubTitle
            cell.lbl_ArticleDate.text = dateTimeConvertor(str: strDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if((tableView == tvForVideo) || (tableView == tvForNewUpdate)){
            return 142
        }else if ( (tableView ==  tvForExpertContribution) || (tableView ==  tvForArticle) ) {
            return UITableViewAutomaticDimension
        }else {
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if  scrollTag == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoTutorialDetailVC") as!VideoTutorialDetailVC
            let dict = aryFav_TutorialVideo.object(at: indexPath.row)as! NSDictionary
            vc.strcome = "Video"
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (scrollTag == 1){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
            vc.strCome = "FAQ"
            let dict = aryFav_SmartFaq.object(at: indexPath.row)as! NSDictionary
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (scrollTag == 2){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskVC") as! HelpDeskVC
            vc.strViewComeFrom = "ACT"
            let dict = aryFav_Acts.object(at: indexPath.row)as! NSDictionary
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            vc.strActScreenStatus = "FAV_ACT"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (scrollTag == 3){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
            vc.strCome = "Article"
            let dict = aryFav_Articles.object(at: indexPath.row)as! NSDictionary
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (scrollTag == 4){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoTutorialDetailVC") as!VideoTutorialDetailVC
            let dict = aryFav_NewsUpdate.object(at: indexPath.row)as! NSDictionary
            vc.strcome = "News"
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if (scrollTag == 5){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "faq_Rules_ScheduleDetailVC") as! faq_Rules_ScheduleDetailVC
            vc.strCome = "ExpertContribution"
            let dict = aryFav_ExpertContribution.object(at: indexPath.row)as! NSDictionary
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: - UITextFieldDelegate
//MARK: -

extension FavouritesVC: UITextFieldDelegate  {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        if scrollTag == 0 {
            self.searchAutocomplete_Video(Searching: txtAfterUpdate)
        }else if (scrollTag == 1) {
            self.searchAutocomplete_SMARTFAQ(Searching: txtAfterUpdate)
        }else if (scrollTag == 2){
            self.searchAutocomplete_Acts(Searching: txtAfterUpdate)
        }else if (scrollTag == 3){
            self.searchAutocomplete_Article(Searching: txtAfterUpdate)
        } else if (scrollTag == 4){
            self.searchAutocomplete_NewsUpdate(Searching: txtAfterUpdate)
        }else if (scrollTag == 5){
            self.searchAutocomplete_ExpertContribution(Searching: txtAfterUpdate)
        }
        print("\(txtAfterUpdate)")
        return true
    }
    
    func searchAutocomplete_Video(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Description contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (aryFav_TutorialVideo ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForVideo.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryFav_TutorialVideo.mutableCopy() as! NSMutableArray
            self.tvForVideo.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_SMARTFAQ(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Question contains[c] %@ OR Question contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryFav_SmartFaq ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForFAQ.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryFav_SmartFaq.mutableCopy() as! NSMutableArray
            self.tvForFAQ.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    
    
    func searchAutocomplete_Acts(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Title contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryFav_Acts ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForACTS.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryFav_Acts.mutableCopy() as! NSMutableArray
            self.tvForACTS.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_Article(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Description contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryFav_Articles ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForArticle.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryFav_Articles.mutableCopy() as! NSMutableArray
            self.tvForArticle.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_NewsUpdate(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Message contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryFav_NewsUpdate ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForNewUpdate.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryFav_NewsUpdate.mutableCopy() as! NSMutableArray
            self.tvForNewUpdate.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    func searchAutocomplete_ExpertContribution(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        let resultPredicate = NSPredicate(format: "Title contains[c] %@ OR Description contains[c] %@", argumentArray: [Searching, Searching])
        
        if !(Searching.length == 0) {
            let arrayfilter = (aryFav_ExpertContribution ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.array_TV = NSMutableArray()
            self.array_TV = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForExpertContribution.reloadData()
        }
        else{
            self.array_TV = NSMutableArray()
            self.array_TV = aryFav_ExpertContribution.mutableCopy() as! NSMutableArray
            self.tvForExpertContribution.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
    }
    
}



