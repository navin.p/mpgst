//
//  Constant.swift
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreTelephony
import MessageUI
import CoreData



//MARK:
var array_TutorialVideo = NSMutableArray()
var arySmartFAQData = NSMutableArray()
var aryActData = NSMutableArray()
var aryRulesData = NSMutableArray()
var arySchedulesData = NSMutableArray()
var aryArticles = NSMutableArray()
var aryExperContribution = NSMutableArray()
var aryForNews = NSMutableArray()
var aryForSGST = NSMutableArray()
var aryForGoodTax = NSMutableArray()
var aryForServiceTax = NSMutableArray()
var aryForCalculator = NSMutableArray()


var aryFav_TutorialVideo = NSMutableArray()
var aryFav_SmartFaq = NSMutableArray()
var aryFav_Acts = NSMutableArray()
var aryFav_Articles = NSMutableArray()
var aryFav_NewsUpdate = NSMutableArray()
var aryFav_ExpertContribution = NSMutableArray()



let sb = Snackbar()
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var isPad = UIDevice.current.userInterfaceIdiom == .pad
var user_device_token :String = nsud.value(forKey: "DEVICE_TOKEN")as! String
var user_device_type :String = "2"
var user_device_id :String = UIDevice.current.identifierForVendor!.uuidString
var staticViewCome : Int!
var strTagADD : String!

//MARK:
//MARK: IPHONE SCREEN WIDTH AND HEIGHT BOUND CONDITION
let kIS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let kIS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
let kIS_RETINA = UIScreen.main.scale >= 2.0
let kSYSTEM_VERSION = Float(UIDevice.current.systemVersion)

let kSCREEN_X = UIScreen.main.bounds.origin.x
let kSCREEN_Y = UIScreen.main.bounds.origin.y
let kSCREEN_WIDTH = UIScreen.main.bounds.size.width
let kSCREEN_HEIGHT = UIScreen.main.bounds.size.height

let kSCREEN_MAX_LENGTH = max(kSCREEN_WIDTH, kSCREEN_HEIGHT)
let kSCREEN_MIN_LENGTH = min(kSCREEN_WIDTH, kSCREEN_HEIGHT)

let kIS_IPHONE_4_OR_LESS = (kIS_IPHONE && kSCREEN_MAX_LENGTH < 568.0)
let kIS_IPHONE_5 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 568.0)
let kIS_IPHONE_6_OR_7 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 667.0)
let kIS_IPHONE_6P_OR_7P = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 736.0)


////MARK:
//MARK:  WEB SERVICES URL

var BaseURL :String = "http://gst.citizencop.org/gstwebservice.asmx"
var Doc_type_list :String =  BaseURL + "doc_type_list.php"

//MARK:
//MARK: Common Color Code
let colorClear = UIColor.clear
let colorText = UIColor(red: 56.0/255.0, green: 82.0/255.0, blue: 65.0/255.0, alpha: 1)

//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

//MARK:
//MARK: OTHER FUNCTION
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


///
func dateTimeConvertor(str: String , formet : String) -> String {
    var fullNameArr = str.components(separatedBy: ".")
    let strFirst = fullNameArr[0] // First
    
    let dateFormatter = DateFormatter()
    let tempLocale = dateFormatter.locale
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    let date = dateFormatter.date(from: strFirst)!
    dateFormatter.dateFormat = "dd-MM-yyyy"
    dateFormatter.locale = tempLocale
    let dateString1 = dateFormatter.string(from: date)
    dateFormatter.dateFormat = "hh:mm:a"
    dateFormatter.locale = tempLocale
    let dateString2 = dateFormatter.string(from: date)
    let finlString = "Date:\(dateString1)  |  Time:\(dateString2)"
    return finlString
    
}

func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}
func ShakeAnimation(textfiled: UITextField) {
    let shake = CAKeyframeAnimation(keyPath: "transform.translation.x")
    shake.duration = 0.1
    shake.repeatCount = 3
    shake.autoreverses = true
    shake.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    textfiled.layer.add(shake, forKey: "shake")
}


func showSnakbarWithText(strtxt :String) {
    
    sb.sbLenght = .shot
    sb.backgroundColor = UIColor.darkGray
    sb.snackbarHeight = 35
    sb.textColor =  UIColor.white
    sb.createWithText(strtxt)
    sb.show()
    
}
func checkInternetConnection() -> Bool {
    let r = Reachability(hostname: "www.google.com")
    let internetStatus = r?.currentReachabilityStatus
    if internetStatus != Reachability.NetworkStatus.reachableViaWiFi && internetStatus != Reachability.NetworkStatus.reachableViaWWAN {
        return false
    }else{
        return  true
    }
}
func stringFromHtml(string: String) -> NSAttributedString? {
    let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system'; font-size: \(16)\">%@</span>" as NSString, string) as String
    //process collection values
    let attrStr = try! NSAttributedString(
        data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
        options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
        documentAttributes: nil)
    return attrStr
}
func calculateSizeFromString(message : String ,view : UIView) -> CGFloat{
    let maxLabelSize: CGSize = CGSize(width:view.frame.size.width, height: CGFloat(9999))
    let contentNSString = message as NSString
    let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0)], context: nil)
    return expectedLabelSize.height + 100 
}
func calculateSizeFromString1(message : String ,view : UIView) -> CGFloat{
    let myString: NSString = message as NSString
    let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)])
    return size.height
}

func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: "Alert!", message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
         }))
    viewcontrol.present(alert, animated: true, completion: nil)
}

//---------CordDATA---------//

func saveDataInLocal(strEntity: String , strKey : String , data : NSArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    //let data = NSKeyedArchiver.archivedData(withRootObject: data)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch let error as NSError  {
        
    } catch {
        
    }
    
}
func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    do {
        let result = (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
        if (result.count > 0) {
            let person = result[0] as! NSManagedObject
            let deposits_Amount:[AnyObject] = person.value(forKey: "\(strkey)")! as! [AnyObject]
            return deposits_Amount as NSArray
        }
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}
func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}
