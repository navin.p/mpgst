//
//  HomeVC.swift
//  MPGST
//
//  Created by Akhilesh Patidar on 8/10/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    //MARK:
    //MARK: @VARIABLE
    var ary_HeaderImages = NSMutableArray()
    var ary_CollectionData = NSMutableArray()

    //MARK:
    //MARK: @IBOutlet
    @IBOutlet weak var viewPager: ViewPager!
    @IBOutlet weak var pagerController: UIPageControl!
    @IBOutlet weak var collection_home: UICollectionView!
    @IBOutlet weak var pagerHeightConstraint: NSLayoutConstraint!
    //MARK:
    //MARK: DEFAULT FUNCTION
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        ary_HeaderImages = ["banner_3", "banner_2","banner_1"]
        ary_CollectionData = [["title":"TUTORIAL\nVIDEO ","image" :"video"],["title":"RULES","image" :"rules"],["title":"GST\nRATES","image" :"taxt_rate"],["title":"SMART\n FAQ","image" :"faq"],["title":"SCHEDULES","image" :"schedules"],["title":"GST\nCALCULATOR","image" :"calculator"],["title":"ACTS","image" :"law"],["title":"SGST\n NOTIFICATIONS","image" :"sgst_notification"],["title":"HELP\n DESK","image" :"help"],["title":"NEWS\n UPDATES","image" :"news"],["title":"FAVORITES","image" :"favourites"],["title":"ABOUT APP","image" :"about_app"],["title":"ARTICLES","image" :"article"],["title":"NOTIFICATIONS","image" :"notification"],["title":"SHARE APP","image" :"share"], ["title":"ASK OUR EXPERT","image" :"ask_our_expert"],["title":"DISCLAIMER","image" :"disclaimer"],["title":"EXPERT\n CONTRIBUTION","image" :"expert_contribution"]]
        viewPager.dataSource = self;
        viewPager.scrollToPage(1)
        viewPager.animationNext()
        let collectionViewLayout = (self.collection_home.collectionViewLayout as! UICollectionViewFlowLayout)
         collectionViewLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
        collectionViewLayout.scrollDirection = .horizontal
         self.collection_home.collectionViewLayout = collectionViewLayout
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        if  DeviceType.IS_IPHONE_5{
            pagerHeightConstraint.constant = 165.00
        }else if DeviceType.IS_IPHONE_6{
            pagerHeightConstraint.constant = 180
        }else if DeviceType.IS_IPHONE_6P{
            pagerHeightConstraint.constant = 200
        }else if DeviceType.IS_IPAD{
            pagerHeightConstraint.constant = 300
        }else{
            pagerHeightConstraint.constant = 110
        }
    }
    
    //MARK:
    //MARK:  @IBAction
    @IBAction func btnMenu(_ sender : AnyObject) {
        sideMenuVC.toggleMenu()

    }
}

extension HomeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_CollectionData.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homecell", for: indexPath as IndexPath) as! homeCollectionCell
        let dict =  ary_CollectionData[indexPath.row]as! NSDictionary
        cell.lbl_HomeTitle.text = dict["title"]as? String
        cell.lbl_HomeImage.image = UIImage(named:(dict["image"]as? String)!)
        
        if  DeviceType.IS_IPHONE_5{
        }else if DeviceType.IS_IPHONE_6{
        }else if DeviceType.IS_IPHONE_6P{
        }else if DeviceType.IS_IPAD{
        }else{
              cell.lbl_HomeImage.contentMode = .center
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
            return CGSize(width: (self.collection_home.frame.size.width - 30)  / 3, height:(self.collection_home.frame.size.height - 30)  / 3)
    }
    
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.row == 14 {
        
        let message = "Get All your GST Queries Answered with MP GST APP.Download the app from Google Play Store now"
        if let link = NSURL(string: "http://goo.gl/CFmrzD")
        {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }else if (indexPath.row == 8 ){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskVC") as! HelpDeskVC
        vc.strViewComeFrom = "Home"
        self.navigationController?.pushViewController(vc, animated: true)
    }else if (indexPath.row == 10 ){
        
     //   showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Comming Soon...", viewcontrol: self)

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FavouritesVC") as! FavouritesVC
        vc.strViewComeFrom = "Home"
        self.navigationController?.pushViewController(vc, animated: true)
    }else if (indexPath.row == 11 ){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "About_Disclaimber_DeveloperVC") as! About_Disclaimber_DeveloperVC
        vc.strViewComeFrom = "Home"
        strTagADD = "About App"
        self.navigationController?.pushViewController(vc, animated: true)
    }else if (indexPath.row == 13 ){
        showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Comming Soon...", viewcontrol: self)

//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
//        vc.strViewComeFrom = "Home"
//        self.navigationController?.pushViewController(vc, animated: true)
    }else if (indexPath.row == 16 ){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "About_Disclaimber_DeveloperVC") as! About_Disclaimber_DeveloperVC
        vc.strViewComeFrom = "Home"
        strTagADD = "Disclaimber"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    else{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TutorialVideoVC") as! TutorialVideoVC
        let index  = indexPath.row + 1
        if index == 1{
            staticViewCome = 1
        }else if (index == 2){
            staticViewCome = 4
        }else if (index == 3){
            staticViewCome = 7
        }else if (index == 4){
            staticViewCome = 2
        }else if (index == 5){
            staticViewCome = 5
        }else if (index == 6){
            staticViewCome = 8
        }else if (index == 7){
            staticViewCome = 3
        }else if (index == 8){
            staticViewCome = 6
        }else if (index == 10){
            staticViewCome = 9
        }else if (index == 13){
            staticViewCome = 10
        }else if (index == 15){
            staticViewCome = 17
        }else if (index == 16){
            staticViewCome = 11
        }else if (index == 18){
            staticViewCome = 12
        }
        vc.strViewComeFrom = "Home"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collection_home.contentOffset.x
        let w = self.collection_home.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pagerController.currentPage = (currentPage)
    }
}

extension HomeVC:ViewPagerDataSource{
    func numberOfItems(_ viewPager:ViewPager) -> Int {
        return 3;
    }
    
    func viewAtIndex(_ viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = view;
        var img : UIImageView?
        if(newView == nil){
            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  self.view.frame.height))
            img = UIImageView(frame: newView!.bounds)
            img!.tag = 1
            img!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
            newView?.addSubview(img!)
        }else{
            img = newView?.viewWithTag(1) as? UIImageView
        }
        let imageName = ary_HeaderImages.object(at: index)
        let image = UIImage(named: imageName as! String)
        img?.image = image
        img?.contentMode = .scaleToFill
        
        return newView!
    }
    func didSelectedItem(_ index: Int) {
        //print("select index \(index)")
    }
}




